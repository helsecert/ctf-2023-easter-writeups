Siden oppgaven går på maldocs bebynner vi med oletools.
Oletools dumper ut en makrokode. Entrypunktet for maldocet er funksjonen "AutoOpen" som kjører når dokumentet åpnes (om man aktiverer content...)
Følger man koden gjennom ser vi at funksjonen eggjakt() kjører.
I eggjakt kjøres følgende kode 

`Debug.Print DecodeBase64(eggeskall) + vbCrLf + eggehvite; vbCrLf; DecodeBase64(eggeskall)`

Debug print er statements som printer til debugvinduet til VBA. Det er noe man har  muligheten til å få opp om man debugger makroen i office.
Den kaller `DecodeBase64` på output fra funksjonen `eggeskall`. `DecodeBase64` er enkelt base64-dekoding, bare implementert som kode istedefor innebygget funksjon.

`vbCrLf` er linjeskift


<details>
<summary>I midten har vi `eggehvite` som (bare basert på navnet) ser mer interessant ut.</summary>

```
Function eggehvite()
Dim Eggejakt As Boolean

If Eggejakt Then
    eggehvite = "                  _ /"
    eggehvite = eggehvite + Eggeplomme(1)
    eggehvite = eggehvite + "\ "
    eggehvite = eggehvite + vbCrLf + "|_  _ | _ _  _ |_(_( "
    eggehvite = eggehvite + Eggeplomme(2)
    eggehvite = eggehvite + "  )" + vbCrLf
    eggehvite = eggehvite + "| )(-`|_)(-`(_ |_|  \"
    eggehvite = eggehvite + Eggeplomme(3) + "/ "
Else
    eggehvite = "                  _ /"
    eggehvite = eggehvite + "                                                           " + "\ "
    eggehvite = eggehvite + vbCrLf + "|_  _ | _ _  _ |_(_( "
    eggehvite = eggehvite + "                                                          " + "  )" + vbCrLf
    eggehvite = eggehvite + "| )(-`|_)(-`(_ |_|  \"
    eggehvite = eggehvite + "                                                           " + "/ "
End If

End Function
```
</details>


VBA-funksjoner returnerer det som tilordnes til variabelen med samme navn som funksjonen. Vi ser at avhengig av variabelen `Eggejakt` kjøres en If/Else. `Eggejakt` er definert som en bool, men faktisk ikke satt. Mao vil `Else` kjøre. Sammenligner vi de ser vi at forskjellen er at `Eggeplommen` ikke taes med inn. 

<details>
<summary>Funksjonen `Eggeplomme`</summary>

```
Function Eggeplomme(i As Integer)


Select Case i
    Case 3
        Eggeplomme = "|  |/--\|__|__/\__/\__\__/|   |    \/ /--\| \ |  ||| \|\__)"
    Case 1
        Eggeplomme = "            __  __  __ __  __  __          __           __ "
    Case 2
        Eggeplomme = "|\/| /\ |  |  \/  \/  /  \|__)|__)\  / /\ |__)|\/|||\ |/ _ "

End Select

End Function
```
</details>



<details>
<summary>Vi ser at avhengig av input (1-3) returnerer den tilsvarende case. Setter vi sammen dette blir det til:</summary>


```
            __  __  __ __  __  __          __           __ 
|\/| /\ |  |  \/  \/  /  \|__)|__)\  / /\ |__)|\/|||\ |/ _ 
|  |/--\|__|__/\__/\__\__/|   |    \/ /--\| \ |  ||| \|\__)
```
`MALDOCOPPVARMING`
</details>


<details>
<summary>Om vi setter sammen all tekst fra i debugprint (med `Eggeplomme` på) får vi:</summary>


```
           _,,gg,,_                                 _,,gg,,_
        ,a888P88Y888a,                           ,a888P88Y888a,
      ,d"8"8",YY,"8"8"b,                       ,d"8"8",YY,"8"8"b,
     d",P'd' d'`b `b`Y,"b,                    d",P'd' d'`b `b`Y,"b,
   ,P",P',P  8  8  Y,`Y,"Y,                 ,P",P',P  8  8  Y,`Y,"Y,
  ,P ,P' d'  8  8  `b `Y, Y,               ,P ,P' d'  8  8  `b `Y, Y,
 ,P ,P_,,8ggg8gg8ggg8,,_Y, Y,             ,P ,P_,,8ggg8gg8ggg8,,_Y, Y,
,8P"""""""''      ``"""""""Y8,           ,8P"""""""''      ``"""""""Y8,
d'/~\    /~\    /~\    /~\  `b           d' ,a8a,    /\ /\    ,a8a,  `b
8/   \  /   \  /   \  /   \  8           8 ,P" "Y,    ( )    ,P" "Y,  8
8 ,8, \/ ,8, \/ ,8, \/ ,8, \/8           8,P'   `Y, .( o ). ,P'   `Y, 8
8 "Y" /\ "Y" /\ "Y" /\ "Y" /\8           8P'/\ /\`Y,   _   ,P'/\ /\`Y,8
8\   /  \   /  \   /  \   /  8           8'  ( )  `Y, (_) ,P'  ( )  `Y8
8 \_/    \_/    \_/    \_/   8           8 .( o ). `Ya   aP' .( o ). `8
8                            8           8 =-=-=-=   "YaP"   =-=-=-=  8
Y""""YYYaaaa,,,,,,aaaaPPP""""P           Y""""YYYaaaa,,,,,,aaaaPPP""""P
`b ag,   ``""""""""''   ,ga d'           `b ag,   ``""""""""''   ,ga d'
 `YP "b,  ,aa,  ,aa,  ,d" YP'             `YP "b,  ,aa,  ,aa,  ,d" YP'
   "Y,_"Ya,_)8  8(_,aP"_,P"                 "Y,_"Ya,_)8  8(_,aP"_,P"
     `"Ya_"""    """_aP"'                     `"Ya_"""    """_aP"'
        `""YYbbddPP""'                           `""YYbbddPP""'
                  _ /            __  __  __ __  __  __          __           __ \ 
|_  _ | _ _  _ |_(_( |\/| /\ |  |  \/  \/  /  \|__)|__)\  / /\ |__)|\/|||\ |/ _   )
| )(-`|_)(-`(_ |_|  \|  |/--\|__|__/\__/\__\__/|   |    \/ /--\| \ |  ||| \|\__)/ 
           _,,gg,,_                                 _,,gg,,_
        ,a888P88Y888a,                           ,a888P88Y888a,
      ,d"8"8",YY,"8"8"b,                       ,d"8"8",YY,"8"8"b,
     d",P'd' d'`b `b`Y,"b,                    d",P'd' d'`b `b`Y,"b,
   ,P",P',P  8  8  Y,`Y,"Y,                 ,P",P',P  8  8  Y,`Y,"Y,
  ,P ,P' d'  8  8  `b `Y, Y,               ,P ,P' d'  8  8  `b `Y, Y,
 ,P ,P_,,8ggg8gg8ggg8,,_Y, Y,             ,P ,P_,,8ggg8gg8ggg8,,_Y, Y,
,8P"""""""''      ``"""""""Y8,           ,8P"""""""''      ``"""""""Y8,
d'/~\    /~\    /~\    /~\  `b           d' ,a8a,    /\ /\    ,a8a,  `b
8/   \  /   \  /   \  /   \  8           8 ,P" "Y,    ( )    ,P" "Y,  8
8 ,8, \/ ,8, \/ ,8, \/ ,8, \/8           8,P'   `Y, .( o ). ,P'   `Y, 8
8 "Y" /\ "Y" /\ "Y" /\ "Y" /\8           8P'/\ /\`Y,   _   ,P'/\ /\`Y,8
8\   /  \   /  \   /  \   /  8           8'  ( )  `Y, (_) ,P'  ( )  `Y8
8 \_/    \_/    \_/    \_/   8           8 .( o ). `Ya   aP' .( o ). `8
8                            8           8 =-=-=-=   "YaP"   =-=-=-=  8
Y""""YYYaaaa,,,,,,aaaaPPP""""P           Y""""YYYaaaa,,,,,,aaaaPPP""""P
`b ag,   ``""""""""''   ,ga d'           `b ag,   ``""""""""''   ,ga d'
 `YP "b,  ,aa,  ,aa,  ,d" YP'             `YP "b,  ,aa,  ,aa,  ,d" YP'
   "Y,_"Ya,_)8  8(_,aP"_,P"                 "Y,_"Ya,_)8  8(_,aP"_,P"
     `"Ya_"""    """_aP"'                     `"Ya_"""    """_aP"'
        `""YYbbddPP""'                           `""YYbbddPP""'

```
</details>



<details>
<summary>Flagg</summary>


Flagg: helsectf{MALDOCOPPVARMING}
</details>


