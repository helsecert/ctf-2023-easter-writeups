# Writeup getting going

Olevba gir oss følgende makro:


´´´
    Private Sub Document_Open()\n
    flagg = "aGVsc2VjdGZ7dmFuc2tlbGlnaGV0c2dyYWQt"
    flagg = flagg + ActiveDocument.StoryRanges(1)
    flagg = flagg + "b3BwfQ=="


    End Sub
´´´
Altså: ´helsectf{vanskelighetsgrad-´ og ´opp}´ med ´ActiveDocument.StoryRanges(1)´ i midten.
De to base64-blokkene dekoder fint sammen, men gir ikke riktig flagg. Dette siden det hentes ut en streng til fra body til activedocument. Nemlig c2tydXMt. Eller "skrus-".

Fullt flagg er dermed:
´helsectf{vanskelighetsgrad-skrus-opp}´
    