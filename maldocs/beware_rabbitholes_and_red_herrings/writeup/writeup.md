# Writeup beware rabbitholes and red herrings

Som oppgavetittelen hinter om statser denne hardt på misdirection.
Den sier den bygger et flagg og kjører noe lagring av fil med powershell. Alt dette er misdirection og kaninhull.

Følger man kontrollflyten videre ser man at siste funksjon som kjøres bygger stringen cimplicated_control_flow ved hjelp av data fra dokumentet. (Den henter data på samme måte som i oppgave "getting going"). Den henter deretter ut enkelt bokstaver fra dataen som bygges til en string.

Setter man sammen alle disse får man strengen helsectf{follow_the_flow}