# app.py
from flask import Flask
from flask import request, send_file

FLAG = "DSC0002.jpeg"
app = Flask(__name__)

@app.route("/")
def home():
    return "<p>Nothing to see here...!</br>Move along...</p>"

@app.route("/DSC0002.jpeg")
def flag():
    if not 'User-Agent' in request.headers:
        try:
            return send_file("./"+FLAG, download_name=FLAG, mimetype="image/jpeg")
        except Exception as e:            
            return "<p>ups, noe gikk galt. kontakt en CTF admin</p>"        
    ua = request.headers.get('User-Agent').lower()
    if len(ua) == 0:
        return "<p>en tom UA?<p>"
    if "mobile" in ua:
        return "<p>du prøver å finne flagg fra mobilen?</p>"
    if "edga/" in ua or "edg/" in ua:
        return "<p>seriøst? edge?</p>"
    if "curl" in ua:
        return "<p>Curl fra powerpoint??<p>"
    if "wget" in ua:
        return "<p>wget + PPT != sant<p>"
    if "chrome/" in ua:
        return "<p>hvordan fikk du til chrome fra powerpointen?</p>"
    if "firefox/" in ua:        
        return "<p>hvordan fikk du til firefox fra powerpointen?</p>"
    if "powershell/" in ua:
        return "<p>kjøres det powershell fra PPTen??</p>"
    return "<p>hvilken UA setter windowsmetoden fra maldocen??</p>"
