# app.py
from flask import Flask
from flask import request, send_file

MALDOC_DL = "2e04d0e73340190b40f3e37882e1d9d8.dll"
app = Flask(__name__)

@app.route("/")
def home():
    return "<p>So it begins, the great battle of our time.</p>"

@app.route("/2e04d0e73340190b40f3e37882e1d9d8.dll")
def enmaldocfil():
    try:
        return send_file("./" + MALDOC_DL, download_name=MALDOC_DL, mimetype="Assembly")
    except Exception as e:
        return "<p>ups, noe gikk galt. kontakt en CTF admin</p>"
