# Ctf 2023 Easter Writeups

# HelseCTF 2023
Registrering -- Logg inn

Årets CTF går av stabelen 20. mars kl. 12.00 og varer til 27. mars kl.12.00. Da presenteres vinnerne av årets trofé: en kyllingkopp

# Capture The Flag

En CTF er en konkurranse (et spill) og har som regel oppgaver med forskjellige aspekter av IT sikkerhet. Det kan være å komme seg forbi en sperre på en nettside, grave i nettverksdata, reverserer et dataprogram eller knekke et passord. Noen ganger kan det være rene "gåter" som må forstås og løses. En god CTF oppgave er lett å forstå men vanskelig å løse. Ett av formålene med en CTF er læring.

Når man har løst en oppgave blir man belønnet med et flagg. Flagget leveres inn for å score poeng og måle krefter med de andre deltagerne på Belønningstavlen. Alle oppgaver skal ha et gjenkjennelig flagg som følger flaggformatet:

`helsectf{ord,setning,bokstaver,tall,spesialtegn}`

Flagget er en lesbar tekststreng med kjent start/slutt og kan inneholde bokstaver, tall og spesialtegn. Flagget vil aldri inneholde mellomrom. Noen eksempler på flagg kan være (ingen av disse er gyldige):

```
    helsectf{En_g0d_CTF_forLenG3r_l1vE7}
    helsectf{KodingKanVaereGoy!}
    helsectf{d3773_3r_06544_37_f1466}
    helsectf{0123456789}
    helsectf{____,o/_____}
```

# Generell informasjon og regler:

    Registrering åpner fredag 17. mars kl 12
    Begynner mandag 20. mars kl 12
    Avslutter mandag 27. mars kl 12

    Individuell konkurranse
    Åpen for alle
    Ingen premier, men de aller flinkeste vil kanskje få en oppmerksomhet i posten
    Primært på norsk, med noe engelsk
    Vi ser helst at ingen deler flagg


CTF er en sosial sport! Kom og bli med på vår Discord server for å prate med andre deltakere: https://discord.gg/b3zS2QrrU9

Lykke til!