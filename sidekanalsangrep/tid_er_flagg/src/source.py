
from Crypto.Util.number import bytes_to_long
import time
from flag import flag
FLAG = bytes_to_long(flag)

def access_bit(i):
    time.sleep(((FLAG>>i)&1)*0.5)

def Challenge():
    print("""Velkommen til første oppgave i en serie med sidekanalsangrep.

Sidekanalsangrep er en type angrep som går ut på å innhente ekstra informasjon uten å få direkte svar, hacke eller knekke noe.
Dette kan være alt fra å måle strømtrekk, synkronisering av ulike typer informasjon eller måle tid.

Ofte er årsaken en grov feil, forenkling eller optimalisering i implementasjonen av en algoritme, mens selve standarden, protokollen eller algoritmen er uten feil.
    
I denne oppgaven vil du greie å lekke bittene til flagget hvis du setter av litt _tid_ til å løse den.
    """)    
    while True:
        bit = input(f"Hvilket bit skal vi aksessere? (bitindex er et tall mellom 0 .. {FLAG.bit_length()}) ")
        try:
            bit = int(bit)
        except Exception:
            print(f"Du må oppgi bitindex, som er et tall mellom 0 .. {FLAG.bit_length()}")
            continue

        if bit < 0 or bit > FLAG.bit_length():
            print(f"Du må oppgi bitindex, som er et tall mellom 0 .. {FLAG.bit_length()}")
            continue
        
        print(f"vent litt, vi skal aksessere bitindex {bit}")
        access_bit(bit)
        print(f"done")
        print()

if __name__ == "__main__":
    c = Challenge()