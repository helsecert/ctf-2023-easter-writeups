"""
Oppgaven demonstrerer Spectre (CVE-2017-5753, CVE-2017-5715) som er et tidsbasert sidekanalsangrep for
å lekke informasjon fra privat minneområde.

Det finnes mange gode forklaringer på denne sårbarheten på nett, og det kan være fint å sette seg skikkelig
inn i bakgrunnen for hvorfor den oppstod (ville ha økt ytelse), hvordan den reelt kan eksekveres lokalt
på en datamaskin og eksakt gangen i angrepet. Oppgaven her er en veldig forenklet utgave av Spectre
sårbarheten, og den er noe tilpasset slik at det er mulig å gjøre den over nettverk.

Spectre er en lokal sårbarhet, du må kunne kjøre et program på en datamaskin for å utnytte den. En datamaskin
har mange lag med caching, mens her bruker vi bare 1 cache. Vi tar ikke med trening av branch prediction (gren
prediksjon) eller hvordan den spekulative utførelsen normalt ville skjedd. Vi legger på en sleep når vi får
cache-miss og må lese fra "fysisk minne", dette slik at vi får en målbar differanse mellom cache hit og cache
miss. For å renske cachen trenger man bare å sende inn en ugyldig minne adresse.

Sikkerhet i CPU instruksjoner fordrer at CPUen gjør instruksjonene riktig. Optimaliseringen
"spekualtiv utførelse" gjør at CPUen kan gjøre feil beregninger, men forkastet de i etterkant,
slik at svaret alltid blir riktig. SPECTRE handler om å utnytte dette slik at CPUen gjør
eksakt og ønskede feilene, men hvor det etterlates innhold i cache som ikke blir ryddet.
Angrepet går ut på at man måler forskjell i svartid på oppslag mot cache, og på den måten kan
man lekke informasjon fra et privat minneområde.

_FLAG1_ ligger i public minneområde, og kan lese ut 1 byte om gangen.
_FLAG2_ må lekkes fra privat minneområde med et "SPECTRE angrep".

```python
    def fetch_byte(self,memory_address):
        ret = self.read_from_memory(memory_address)
        if memory_address < 512:
            self.read_from_memory(ret*512)
            return 0
        return ret
```

# flag1
Når vi ber om en minne adresse så vil vi først lese ut en byte fra den adressen. Hvis posisjonen
er >= 512 så vil vi returnere svaret. Dette gjør at vi fritt kan lese alt innhold fra minne adresse
512 og oppover, og få returnert innholdet.

# flag2
Hvis minne adressen var < 512, beskyttet minneområde, så får vi alltid tilbake "0". Koden vil i tillegg
lese fra minnet i posisjon `<ret>*512`, og forkaster resultatet. "ret" er desimal veriden til byten
som ligger i minne adressen vi ønsker å lese. Dette er for å simulere "speculative execution" som SPECTRE
utnytter mens sjekken på "<512" er en forkortet "branch prediction".

```python
    @lru_cache(maxsize=512)
    def read_from_memory(self,memory_address):        
        time.sleep(1. / 1e3)
        try:
            assert memory_address >= 0 and memory_address < len(self.memory)
            return self.memory[memory_address]
        except:
            raise Exception("ugyldig minneadresse",memory_address)
```

Denne funksjonen er dekorert med `lru_cache`. Dette fungerer slik at hvis vi kaller funksjonen med
et argument som ligger i cache, så får vi svaret fra cache -- vi slipper å eksekvere koden inne i funksjonen.
Hvis argumentet ikke ligger i cache så vil koden eksekvere. Angrepet består i å måle forskjell i tid,
på om vi har en cache miss eller cache hit.

Som eksempel kan vi nå tenke oss at i minne adresse 0 finnes bokstaven "h" (desimal 104). Hvis vi kaller
funksjonen `fetch_byte(0)` så vil vi først lese minne adresse 0, og får `ret=104`. Minne adresse 0 havner
nå i cache. Siden `minne_adresse < 512` så vil vi også lese minne adresse `ret*512 = 104*512 = 53248`, og
lagre den i cache mens resultatet forkastes og vi får returnert "0". Vi har nå 2 ting i cache: minneadresse 0
og minneadresse 53248.

Hvordan kan vi nå finne ut hva som ligger i minne adresse 0? Vi får jo ikke noe svar (return 0 = ikke noe svar).

Angrepet består i å forsøke (bruteforce) alle mulige tegn samtidig som vi måler tiden. Vi ber om innholdet til
minneadressene `32*512`, `33*512`, .., `127*512`. Hvis vi nå ser på tidsmålingene for disse så er det sannsynlig
at `104*512` vil ha en kortere svartid enn resten siden den allerede ligger i cache. Det er et usikkerhetsmoment:
nettverk. Størsteparten av tiden du måler er nettverkspakkene som går frem og tilbake. Målingene vil være utsatt
for støy, ikke minst hvis man kjører dette fra en laptop på et dårlig eller trafikkert Wifi nettverk.

Vi kan ha en nettverksglipp eller treigthet akkurat når vi måler det tegnet som er riktig og det kan gjør at et
annet tegn får lavest svartid. Vi må derfor gjøre mange målinger og se om vi kan finne ut riktig svar til tross
for variasjoner i målingene.

Det er veldig viktig at man rensker cachen før hvert forsøk.

Jeg valgte å lage en frekvenstabell på alle målingene jeg foretar. Jeg gjør mange målinger for samme minneadresse
og hvis jeg har hatt samme tegn n-antall ganger så antar jeg det er ritktig tegn.

Solveren er skrevet slik at man skal forstå angrepet slavisk. Det er helt sikkert flere optimaliseringen som kan
gjør angrepet raskere. Hvis man har en god og stabil kobling, og/eller hvis veldig få gjør oppgaven samtidig 
så kan man kanskje greie seg med færre tester. Parallelle koblinger går ikke, da FLAG2 blir skrevet med et random offset.

Siden FLAG2 blir plassert i random minneområde <=10 og siden lengden av "helsectf{" er 9 så kan vi spare tid ved å starte
på offset 9 og lese fremover.
"""
from Crypto.Util.number import *
from pwn import remote
from operator import itemgetter
import time

#io = remote("localhost", port=8002)
io = remote("helsectf2023-6ac4e1c6d8855c1bd96a-minnespekter.chals.io", 443, ssl=True)

def measure_time(i):
    io.recvuntil(b"Hvilken minneadresse vil du lese? ")
    t0 = time.time() 
    io.sendline(str(i).encode())
    io.recvuntil(b">")
    ret = int(io.recvline().split(b" ")[2].strip().decode(), 16)    
    _t = int((time.time()-t0)*1e6)
    #print("_t", chr(i), _t)
    return _t, ret
    
# flagg1 leses rett ut fra ubeskyttet minneområde.
print("flagg1", "".join([chr(measure_time(i)[1]) for i in range(512,612)]))

# for flagg2 må vi lekke data fra offset 0
data = ""
for pos in range(9,100): # vi kan skippe len("helsectf{"") pga random minneplassering for å spare _tid_
    freq = {0:0}
    
    # Hvis vi får samme verdi 5 ganger så er vi fornøyd!
    while max(freq.values()) < 5:
        io.sendline(b"clear")                                                           # sender ugyldig minneadresse slik at cache blir tømt        
        measure_time(pos)                                                               # tvinger "speculative exection" til å laste <tegn i pos som skal lekkes>*512 i cache
        candidates = [[char, measure_time(char*512)[0]] for char in range(0x20,0x7f)]   # måler svartid for alle tegn
        candidate = min(candidates,key=itemgetter(1))[0]                                # kortest svartid = lå allerede i cache
        if not candidate in freq: freq[candidate] = 0
        freq[candidate] += 1                                                            # oppdaterer frekvenstabell.
        print("candidate=", candidate, freq)    

    data += chr(max(freq, key=lambda key: freq[key]))                                   # velger tegnet som flest ganger ble målt med lavest svartid flest ganger, dvs 5.
    freq.pop(0)
    print("candidates=", freq)    
    print("memory> ",data)
    if "}" in data:
        break