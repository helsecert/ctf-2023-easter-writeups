import time
import secrets
import numpy as np
from functools import lru_cache
from flag import FLAG1, FLAG2

class Minnespekter():
    def __init__(self):        
        self.memory = np.random.randint(0x20, 0x7f, (512 + (255*512)))
        self.write(FLAG1,512)
        self.write(FLAG2,secrets.randbelow(10))

    def write(self, data, offset):
        # kopierer data inn i minnet, overskriver fra <offset..>
        for i in range(len(data)):
            self.memory[offset+i] = data[i]

    def fetch_byte(self,memory_address):
        ret = self.read_from_memory(memory_address)
        if memory_address < 512:
            self.read_from_memory(ret*512)
            return 0
        return ret

    @lru_cache(maxsize=512)
    def read_from_memory(self,memory_address):        
        time.sleep(1. / 1e3)
        try:
            assert memory_address >= 0 and memory_address < len(self.memory)
            return self.memory[memory_address]
        except:
            raise Exception("ugyldig minneadresse",memory_address)

class Challenge():
    minne = Minnespekter()

    print("""I denne oppgaven om sidekanalsangrep har vi laget en forenklet utgave av en kjent sårbarhet.

== Oversikt over minnet ==
00000000 - 000001ff        beskyttet minneområde med privat data
00000200 - 0001ffff        ikke-beskyttet minneområde

Du kan ikke lese data fra beskyttet minneområde direkte, men kanskje det finnes andre måter å lese ut flagget på?
    """)

    while True:
        try:
            minneadresse = int(input("Hvilken minneadresse vil du lese? "))
            assert minneadresse >= 0 and minneadresse < len(minne.memory)
        except Exception:
            print("Feil! ugyldig minneadresse")
            minne.read_from_memory.cache_clear()
            continue

        val = minne.fetch_byte(minneadresse)
        print(">", minneadresse.to_bytes(4,"big").hex(), hex(val)[2:].rjust(2, "0"))

if __name__ == "__main__":
    c = Challenge()