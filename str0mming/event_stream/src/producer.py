from kafka import KafkaProducer
from Crypto.Random.random import getrandbits
import json  
import random
import string
alfa = list(map(ord, string.ascii_letters + string.digits + string.punctuation))

#_host="localhost:9092"
#_host="ec2-13-48-132-245.eu-north-1.compute.amazonaws.com:9092" # liten, backup
_host="ec2-13-49-134-84.eu-north-1.compute.amazonaws.com:9092" # stor
_username="admin"
_password="e86c40d256ca0e919cec6cb3"
producer = KafkaProducer(
    bootstrap_servers=_host,
    security_protocol="SASL_PLAINTEXT",
    sasl_plain_username=_username,
    sasl_plain_password=_password,
    sasl_mechanism="PLAIN",    
    value_serializer=lambda v: json.dumps(v).encode('utf-8'))

def oppgave1():
    _topic = "event_stream"
    FLAG1 = "helsectf{3n_STr0m_meD_eV3ntS}"
    events=[]
    for ip in open("compromised-ips.txt","r").readlines():
        events.append({"feed":"emergingthreats","source":"https://rules.emergingthreats.net/blockrules/compromised-ips.txt","action":"create", "intel":[{"type":"ip", "value": ip.strip()}]})
    events = events[0:len(events)//2] + [{"feed":"helsectf","source":"https://helsectf2023-6ac4e1c6d8855c1bd96a.ctfd.io/","action":"create", "intel":[{"type":"flag","value":FLAG1}]}] + events[len(events)//2:]    
    for e in events:
        future = producer.send(_topic, e)
        result = future.get(timeout=5)
        #print(">", e)
    producer.flush()

def oppgave2():
    _topic = "event_sourcing"
    partitions = 10    
    state = list(b"helsectf{??????????????????????????????????????}")
    FLAG2 = list(b"helsectf{stream_table_duality_by_event_sourcing}")        
    _tid = 1679483480
    events = []
    noise = {}

    # laster inn initial state slik at man får et inntrykk av flagget
    for k,v in enumerate(state):
        partition = random.randint(0, partitions-1)
        events.append( [_tid,partition,k,v,"create"] )

    # vi begynner å endre state hvor vi har feil (støy) innhold.
    # vi må over tid endre slik at state == flagget slik at det er mulig å løse oppgaven.
    def_noise_lev = 1
    noise_lev = def_noise_lev
    for i in range(1000):
        partition = random.randint(0, partitions-1)
        k = random.randint(9, len(FLAG2)-2)
        if random.randint(0, 1+len(noise))         <= noise_lev:
            v = random.choice(alfa)
            noise[k] = v            
        else:
            if len(noise) > 0:
                k,_ = random.choice(list(noise.items()))            
                noise.pop(k,"None")
            v = FLAG2[k]
            
                
        _tid += random.randint(2,10)
        state[k] = v
        print(_tid, "".join(chr(x) for x in state), partition, k, v)
        events.append( [_tid,partition,k,v,"update"] )

        if len(noise) == 0 and not ord("?") in state and noise_lev == def_noise_lev:
            noise_lev = 5          
            print("! solveable")              

    assert noise_lev == 5 # da er oppgaven løsbar.

    for e in events:
        _tid,partition,k,v,_action = e
        j = {"feed":"helsectf","source":"https://helsectf2023-6ac4e1c6d8855c1bd96a.ctfd.io/","action":_action,"intel":[{"type":"flag","value":chr(v)}]}
        print(_tid, partition, k, j)
        future = producer.send(_topic, j, key=chr(k).encode(), partition=partition, timestamp_ms=_tid*1000)
    producer.flush()



def oppgave3():
    _topic_ciphertext = "event_ciphertext"
    _topic_key = "event_key"
    partitions = 10    
    FLAG3 = "Her kommer en sensitiv melding: helsectf{t0_str0mmer_ble_til_3n}"
    for k,pt in enumerate(FLAG3):
        key = getrandbits(8)
        print(key,pt)
        partition = random.randint(0, partitions-1)        
        producer.send(_topic_key, {"key_byte": "0x"+bytes([key]).hex()}, key=chr(k).encode(), partition=partition)
        producer.send(_topic_ciphertext, {"encrypted_byte": "0x"+bytes([ord(pt)^key]).hex()},key=chr(k).encode(), partition=partition)
    producer.flush()

oppgave1()
oppgave2()
oppgave3()
