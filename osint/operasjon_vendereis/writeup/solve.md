1. Gå inn på https://twitter.com/helsectf
2. Gå til følgere og finn franskmannen: https://twitter.com/vivant_pixel
	- Den første Tweeten han publiserte har SHA256 for BTC-transaksjonen.
	- Kommentaren hinter til at det skjedde noe galt første gang avsender prøvde å sende BTC.
4. Søk etter BTC-transaksjonen på en tjeneste som tilbyr dette. Eks. blockchair.com:
	- https://blockchair.com/bitcoin/transaction/9b0c8860f3f974fc4c796cf67d930e4a6de151c599d02d89573fa670751bd2d4
5. Gå til avsender sin wallet (ref. hintet): https://blockchair.com/bitcoin/address/bc1qrg0al5whtweplhccge2j5sn8zl3ppfl46jent7
6. Se tidligere transaksjon, hvor man har en OP_RETURN på 0.0BTC: https://blockchair.com/bitcoin/transaction/27a73f2af8e565dddcd55291e216bad593ca26fdcc09d667ad2c4679b104f6d4
7. OP_RETURN inneholder: `68656c73656374667b53757033725f5333637265745f426c6f636b636834316e5f4d53477d`
	- Dekodet blir dette: `helsectf{Sup3r_S3cret_Blockch41n_Mess4ge}`

Oppgavetittelen er en referanse til OP_RETURN.

# Hint (for å peke folk mer mot CryptoPixel brukeren på Discord) som Notification på CTFd/discord:
Kunstnerisk venn på besøk
    Påskeharen har fått besøk av en kunstnerisk venn på Discord.