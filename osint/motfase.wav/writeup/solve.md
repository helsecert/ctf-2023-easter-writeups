1. Last ned filen sons_de_canard.wav.
2. Åpne filen i Audacity.
3. Se etter metadata i filen. Man finner da at author er: aHR0cHM6Ly9mcmVlbXVzaWNhcmNoaXZlLm9yZy9tdXNpYy9Mb216X19MZXpldC9Nb3RoZXJfQnJhaW4vTG9tel9hbmRfTGV6ZXQtTW90aGVyX0JyYWluXzA0X0dsaXRjaF8zLw==.
4. Dette er base64, og det dekoder til: 
                                        https://freemusicarchive.org/music/Lomz__Lezet/Mother_Brain/Lomz_and_Lezet-Mother_Brain_04_Glitch_3/
5. Last ned filen fra denne siden og legg den til som en ny track i Audacity, slik at den spiller side-om-side med sons_de_canart.wav.
6. Bruk "Effect" -> "Invert" på den nye tracken for å invertere lyden. Dette fjerner støyen slik at man kan høre hva som blir sagt. Dette er samme konsept som "Active Noice Cancellation".
7. Hør på lyden og hør hva som blir sagt. Se solve.mp3 for lydfilen som du skal få ut til slutt. Her er flagget. Vi godtok disse flaggene:
 - helsectf{audio_interferens}
 - helsectf{audiointerferens}
 - helsectf{audiointerferens}
 - helsectf{audio_interference}
 - helsectf{audio interference}
 - helsectf{audiointerference}

Oppgavetittelen er en referanse til aktiv støydemping.
