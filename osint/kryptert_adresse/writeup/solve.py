#146,helsectf{LCG_i_solidity!}
"""
Encryptor Contract: 0x121fA716676a425192422204ce723d6e50acc375

Vi finner kontrakten, og ser at vi kun kan kjøre `get_encrypted_flag`. Her får vi ut et kryptert flag.
Vi kan også finne transaksjoner på kontrakten og vil finne samme informasjon der.

Vi får ut et kryptert flag: 0x72f8a3a1bf16dd894894553a4adf624ed82408090c7908d920c3be82b856e7a60ca3426971897c62d8152304256913a576ce859b8120dcb824b2603b5f

I .sol kontrakten ser vi at encrypt krypterer byte for byte med `state = (0x13*state + 0x32)%0xff` som oppdateres for hver iterasjon. Dette er en LCG: en lineær kongruent generator som gir en pseudo-random nummer serie ut fra en initial tilstand (seed)
  
  s_i = a*s_{i-1} + b (mod n)

Vi kan se på de to første statene:

  s0 (initial state)
  s1 = a*s0 + b   (mod n)

Vi finner nå s0 ved å snu siste ligningen (det er viktig at gcd(a,n) == 1)

  s0 = (s1-b)*a^-1 (mod n)

Nå har vi hele nøkkelen for å XORe mot ct ved at vi kjører vår egen LCG med samme seed.

MEN, vi vet ikke første plaintext byte. Vi kan prøve med "h", men det var det ikke.
Derfor kan vi like gjerne bare bruteforce s0 fra 0..0xff.

En unintended solve er at du finner flagget i klartekst i en av transaksjonene:
https://goerli.etherscan.io/tx/0xf0beeae5c51ef47bacc5c858456ca01ffd147e71aeafe0d37990ef6416bcf65b#statechange
"""
from Crypto.Util.number import inverse,GCD
ct = bytes.fromhex("72f8a3a1bf16dd894894553a4adf624ed82408090c7908d920c3be82b856e7a60ca3426971897c62d8152304256913a576ce859b8120dcb824b2603b5f")              
                          
#s1 = ct[0]^ord("h")
#s0 = ((s1-0x32)*inverse(0x13, 0xff))%0xff
#assert s0 == 146  # <- initial seed i oppgaven.
#s = s0

for seed in range(0, 0xff):
    flag = ""
    s = seed
    for c in ct:
        s = (0x13*s + 0x32)%0xff        
        flag += chr(c^s)                
    print(seed, [flag])
    if "helsectf{" in flag:
        break
# ..   
# ['adressen er helsectf{ikK3_oFt3_AdResS3r_eR_kRyp7eRT_MEd_LCG!}']

