1. Bruk exiftool til å hente ut metadata fra bildene.
2. canes.jpg inneholder følgende hint:
- Author: Whodunit? (who committed the crime?)
- Description: distance de mise au point (focus distance)
- Date/Time Original: 2022:06:16 12:00:00 (skjedde det noe spesielt på denne datoen?)
3. Følgende bilder inneholder usedvanlige focus distanser:
- gratteciel.jpg: 310000.00 - 311000.00 m
- montagnes.jpg: 726000.00 - 727000.00 m
- tunnel.jpg: 1934000.00 - 1935000.00 m
4. Legg disse bildene inn i reverse image search. Man får da lokasjoner som gir (omtrent) følgende koordinater:
- gratteciel.jpg: 51°30'48.6"N 0°05'03.8"W
- montagnes.jpg: 46°06'25.9"N 8°17'21.1"E
- tunnel.jpg: 37°00'53.3"N 7°56'03.8"W
5. Bruk en tjeneste til å plotte radiuser på kart. Eksempel: https://www.mapdevelopers.com/draw-circle-tool.php
- Plott alle radiusene på kartet. Dvs. totalt 6 radiuser. Man vil da finne midtpunktet som viser stedet vi er interessert i: International Criminal Court i Den Haag.
- ![plot.png](Eksempel)
6. Gjør et Google søk på International Criminal Court og datoen, 16 juni 2022. Man må grave litt, men det er fullt mulig å finne artikler om følgende:
- "Russian GRU spy tried to infiltrate International Criminal Court"
- "Authorities say his real name is Sergey Vladimirovich Cherkasov"
7. Referer tilbake til author-taggen og hint i oppgaveteksten - whodunit. Aka. hvem begikk forbrytelsen? Der har man flagget.
- Følgende versjoner av navnet er gyldige flagg, enten med space eller "_" som space:
    - helsectf{Sergey Vladimirovich Cherkasov}
    - helsectf{Sergey_Vladimirovich_Cherkasov}
    - helsectf{Sergey Cherkasov}
    - helsectf{Sergey_Cherkasov}
    - helsectf{Cherkasov Sergey}
    - helsectf{Cherkasov_Sergey}
    - helsectf{Cherkasov}

Oppgavetittel er en referanse til spioner ("mullvarper").


Hint1:
Påskeharen har med ekspert hjelp sett på bildene og synes noen av de har en underlig Focus Distance. Etter litt prøving og feiling greide han eksakt å finne ut hvor bildene var tatt. Men forstår ikke hvordan disse tingene henger sammen. Han har prøvd både kosinus, radius og pi. Han hadde håpet å finne et nytt punkt som vil forklare det hele.

Hint2:
Påskeharens ekspertteam brukte et online verktøy for å plotte inn og tegnet flere store sirkler på et verdenskart.

De ble overrasket da de fant ut at alle sirklene krysset hverandre rett over en kjent bygning en plass i Europa som etterforsker kjeltringer.

De håper nå at det skal være mulig å finne navnet på en kjent mullvarp som i nyere tid fikk en tilknytning til denne bygningen.