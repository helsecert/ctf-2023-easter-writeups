Oppgaven opplyser om "pAAskeharen" og versjonskontroll system. Vi vil gå mot "git".

Du må først løse intro/git_3 for å finne ethereum adressen.
    0xb53449a6cd4D55bD09fD3f4509307139c354cc18

Det står en melding med et testnett. Ethereum har en validator vi kan bruke, huker av for testnett på høyre side så vil man finne Goerli.
Alternativt er å søke opp Ethereum testnett; da finner du 2: sepolia og goerli.
    https://etherscan.github.io/ethvalidate/address

I den finner du den første kontrakten:
    Welcome Contract: 0xC4e40F7492DFE74AfD649a7573eD98d23efc782d

Kan finne flagget på flere måter:

# Argument
Åpne kontrakten: https://goerli.etherscan.io/address/0xc4e40f7492dfe74afd649a7573ed98d23efc782d#code
Se på Constructor Arguments, der ligger flagget som arg[0]

# Kontrakten: kjøre funksjon
Åpne kontrakten: https://goerli.etherscan.io/address/0xc4e40f7492dfe74afd649a7573ed98d23efc782d#readContract
Kan lese ut `flag` eller trykke `solve`.

# Rå binærdump
Åpne kontrakten: https://goerli.etherscan.io/address/0xc4e40f7492dfe74afd649a7573ed98d23efc782d#code
Finn transaksjonen: https://goerli.etherscan.io/tx/0x0e51070f8cd8875319ccce18a03f03fb6c86b2184afa0b048e1021fbef6b0796

Click to show more. Input data View input as UTF-8 ...masse junk ... helsectf{0ppSeT7_aV_b10kkj3dER_eR_lEtT!}
