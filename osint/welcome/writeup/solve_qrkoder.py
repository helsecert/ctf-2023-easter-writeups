from PIL import Image
from pyzbar.pyzbar import decode

for i in range(0, 2159):
    img = Image.open(f'qrcodes/{i}.png')    
    data = decode(img)
    text = data[0][0]
    #print(img.size, text)
    if b"etherum" in text:
        print(i, text)
        break        
