1. Gå inn på https://twitter.com/helsectf
2. Gå til følgere og finn franskmannen: https://twitter.com/vivant_pixel
3. Her har han skrevet om at han har sendt folk en lenke til kunstverkene sine, og at nøkkelen er: Uo8sOE8aWUd3cfHAMOjRQA
4. På HelseCTF-discord i #påskenøtter_2023 har bruker CryptoPixel lagt ut en lenke til kryptert mappe i Mega.nz: https://mega.nz/folder/3zxlHaCT. Bruk nøkkelen fra twitter til å dekyptere innholdet.
   Kan også gå direkte til mega.nz med passordet i URL: https://mega.nz/folder/3zxlHaCT#Uo8sOE8aWUd3cfHAMOjRQA
5. Filen drapeau.txt inneholder flagget.

Tittelen på oppgaven er en referanse til twitter-kontoen: @vivant_pixel.