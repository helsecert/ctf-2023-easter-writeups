Dette er en viderefølging av osint/welcome. 

I Goerli Ethereum accounten finner vi flere tranaksjoner, blant annet til:

  Private Contract: 0x4ca03ddbddf8ed9d2dd383c09df343a29bad0004

Denne gangen hjelper det ikke å dumpe kontrakten binært for å finne flagg. Flag er en privat variabel så vi kan heller ikke bruke remix til å dumpe variabelen direkte eller kalle en funskjon, det er kun set_adresse som er public. Men, når set_address kalles emitter den en event med innholdet.

`set_address` er kalt mange ganger. Vi kan åpne events på kontraken for å se om noe har skjedd:

  https://goerli.etherscan.io/address/0x4Ca03ddbDDF8Ed9d2DD383C09df343a29bad0004#events

Her har det skjedd mye. Jeg brukte et python script med Web3 og en alchemy.com konto:
```
65 ingen adresse her!
66 adresse[6]=01_Ky
67 addressen er: adresse
68 adresse[0]=helse
69 adresse[8]=laNd}
70 adressen er ikke: 1 2 3
71 håper ingen finner denne adressen
87 dette er ikke en adresse
88 adresse[1]=ctf{p
89 adresse
90 adresse[4]=eiEn_
91 helsectf har ingen adresse
92 adresse[5]=1,_00
93 håper ingen spiser adressen til frokost
89 adresse[3]=HaReV
90 hvorfor trenger man en adresse?
91 adresse[7]=lL1nG
92 adressen til eggleveransen er viktig
93 adresse[2]=AAsk3
94 riktig adresse er muligens, ikke: adresseveien 123
```

Vi setter sammen addresse[i] tilbake til en streng, og får:
helsectf{pAAsk3HaReVeiEn_1,_0001_KylL1nGlaNd}


```python
# Setup
from web3 import Web3 # pip install web3

alchemy_url = "..."
w3 = Web3(Web3.HTTPProvider(alchemy_url))

logs = w3.eth.get_logs(filter_params={"address":"...", "fromBlock":"earliest", "toBlock":"latest"})
adresse = [""]*10
for l in logs:
    data = bytes(l["data"].replace(b"\x00",b""))[2:].decode()
    print(l["logIndex"], data)
    if not "adresse[" in data: continue
    exec(data.replace("=","='") + "'")
print("".join(adresse))

```