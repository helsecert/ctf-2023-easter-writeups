#flag="helsectf{eN_3nke1_pa7ch_VaR_n0k}"
flag = open("flag.txt").read().strip()
print("flag=", flag)
ct = ""
for i in range(0, len(flag)):
    ct += flag[(i*7)%len(flag)]
print(f"char str[32] = \"{ct}\";")

# solve
pt = ["?"]*len(ct)
for i in range(0, len(ct)):
    pt[i*7 % len(ct)] = ct[i]    
print("".join(pt))

