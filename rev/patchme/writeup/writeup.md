Vi kan åpne filen i Ghidra for å lese main:
```c

undefined8 main(void)

{
  undefined8 local_68;
  undefined8 local_60;
  undefined8 local_58;
  undefined8 local_50;
  undefined local_48;
  undefined8 local_38;
  undefined8 local_30;
  undefined8 local_28;
  undefined8 local_20;
  undefined4 local_10;
  int local_c;
  
  local_10 = 0;
  local_38 = 0x5f4e736e636b6668;
  local_30 = 0x656c5f376e747d56;
  local_28 = 0x65526133636b5f31;
  local_20 = 0x61705f653068657b;
  if (true) {
    puts("Her skjedde det jo ingen ting.");
  }
  else {
    local_68 = 0;
    local_60 = 0;
    local_58 = 0;
    local_50 = 0;
    local_48 = 0;
    for (local_c = 0; local_c < 0x20; local_c = local_c + 1) {
      *(undefined *)((long)&local_68 + (long)((local_c * 7) % 0x20)) =
           *(undefined *)((long)&local_38 + (long)local_c);
    }
    printf("Hemmelig melding: %s\n",&local_68);
  }
  return 0;
}
```

Vi ser at en patch for å endre `if (true)` til `if (false)` er nok for å få ut flagget. Vi ønsker derfor å patche JNZ (jump not zero) til en JZ (jump zero).

Vi kan kanskje bruke Ghidra til dette, men radare2 er veldig fint til sånt:
```
$ r2 -d ./patchme
[0x7f35da477090]> aaa
[x] Analyze all flags starting with sym. and entry0 (aa)
[x] Analyze function calls (aac)
[x] Analyze len bytes of instructions for references (aar)
[x] Finding and parsing C++ vtables (avrr)
[x] Skipping type matching analysis in debugger mode (aaft)
[x] Propagate noreturn information (aanr)
[x] Use -AA or aaaa to perform additional experimental analysis.
[0x7f35da477090]> s main
[0x00401136]> pdf
            ; DATA XREF from entry0 @ 0x401071
┌ 213: int main (int argc, char **argv, char **envp);
│           ; var int64_t var_60h @ rbp-0x60
│           ; var int64_t var_58h @ rbp-0x58
│           ; var int64_t var_50h @ rbp-0x50
│           ; var int64_t var_48h @ rbp-0x48
│           ; var int64_t var_40h @ rbp-0x40
│           ; var int64_t var_30h @ rbp-0x30
│           ; var int64_t var_28h @ rbp-0x28
│           ; var int64_t var_20h @ rbp-0x20
│           ; var int64_t var_18h @ rbp-0x18
│           ; var int64_t var_8h @ rbp-0x8
│           ; var int64_t var_4h @ rbp-0x4
│           0x00401136      55             push  (rbp)
│           0x00401137      4889e5         rbp = rsp
│           0x0040113a      4883ec60       rsp -= 0x60
│           0x0040113e      c745f8000000.  dword [var_8h] = 0
│           0x00401145      48b868666b63.  rax = 0x5f4e736e636b6668    ; 'hfkcnsN_'
│           0x0040114f      48ba567d746e.  rdx = 0x656c5f376e747d56    ; 'V}tn7_le'
│           0x00401159      488945d0       qword [var_30h] = rax
│           0x0040115d      488955d8       qword [var_28h] = rdx
│           0x00401161      48b8315f6b63.  rax = 0x65526133636b5f31    ; '1_kc3aRe'
│           0x0040116b      48ba7b656830.  rdx = 0x61705f653068657b    ; '{eh0e_pa'
│           0x00401175      488945e0       qword [var_20h] = rax
│           0x00401179      488955e8       qword [var_18h] = rdx
│           0x0040117d      837df801       var = dword [var_8h] - 1
│       ┌─< 0x00401181      7577           if  (var) goto loc_0x4011fa
│       │   0x00401183      48c745a00000.  qword [var_60h] = 0
│       │   0x0040118b      48c745a80000.  qword [var_58h] = 0
│       │   0x00401193      48c745b00000.  qword [var_50h] = 0
│       │   0x0040119b      48c745b80000.  qword [var_48h] = 0
│       │   0x004011a3      c645c000       byte [var_40h] = 0
│       │   0x004011a7      c745fc000000.  dword [var_4h] = 0
│      ┌──< 0x004011ae      eb2c           goto loc_0x4011dc
│     ┌───> 0x004011b0      8b55fc         edx = dword [var_4h]
│     ╎││   0x004011b3      89d0           eax = edx
│     ╎││   0x004011b5      c1e003         eax <<<= 3
│     ╎││   0x004011b8      29d0           eax -= edx
│     ╎││   0x004011ba      99             cd
│     ╎││   0x004011bb      c1ea1b         edx >>>= 0x1b
│     ╎││   0x004011be      01d0           eax += edx
│     ╎││   0x004011c0      83e01f         eax &= 0x1f                 ; 31
│     ╎││   0x004011c3      29d0           eax -= edx
│     ╎││   0x004011c5      89c1           ecx = eax
│     ╎││   0x004011c7      8b45fc         eax = dword [var_4h]
│     ╎││   0x004011ca      4898           cdq
│     ╎││   0x004011cc      0fb65405d0     edx = byte [rbp + rax - 0x30]
│     ╎││   0x004011d1      4863c1         rax = ecx
│     ╎││   0x004011d4      885405a0       byte [rbp + rax - 0x60] = dl
│     ╎││   0x004011d8      8345fc01       dword [var_4h] += 1
│     ╎││   ; CODE XREF from main @ 0x4011ae
│     ╎└──> 0x004011dc      837dfc1f       var = dword [var_4h] - 0x1f
│     └───< 0x004011e0      7ece           if  (var <= 0) goto loc_0x4011b0
│       │   0x004011e2      488d45a0       rax = var_60h
│       │   0x004011e6      4889c6         rsi = rax
│       │   0x004011e9      bf10204000     edi = str.Hemmelig_melding:__s_n ; 0x402010 ; "Hemmelig melding: %s\n"
│       │   0x004011ee      b800000000     eax = 0
│       │   0x004011f3      e848feffff     sym.imp.printf  ()          ; int printf(const char *format)
│      ┌──< 0x004011f8      eb0a           goto loc_0x401204
│      │└─> 0x004011fa      bf28204000     edi = str.Her_skjedde_det_jo_ingen_ting. ; 0x402028 ; "Her skjedde det jo ingen ting."
│      │    0x004011ff      e82cfeffff     sym.imp.puts  ()            ; int puts(const char *s)
│      │    ; CODE XREF from main @ 0x4011f8
│      └──> 0x00401204      b800000000     eax = 0
│           0x00401209      c9             leav
└           0x0040120a      c3             re
[0x00401136]> s 0x00401181
[0x00401181]> wv1 0x74
^D
$ ./patchme.r2
Hemmelig melding: helsectf{eN_3nke1_pa7ch_VaR_n0k}
```

# alt2: reversing
Et annet alernativ er å reverse funksjonen som tygger tekst strengen for å lage flagget.

Først må vi se på konstanten:
```python
from Crypto.Util.number import *
local_38 = 0x5f4e736e636b6668
local_30 = 0x656c5f376e747d56
local_28 = 0x65526133636b5f31
local_20 = 0x61705f653068657b
data = [
  long_to_bytes(local_38)[::-1],
  long_to_bytes(local_30)[::-1],
  long_to_bytes(local_28)[::-1],
  long_to_bytes(local_20)[::-1],      
  ]
print(b"".join(data))
b'hfkcnsN_V}tn7_le1_kc3aRe{eh0e_pa'
```

Og funksjonen som kjører:
```c
for (local_c = 0; local_c < 0x20; local_c = local_c + 1) {
    *(undefined *)((long)&local_68 + (long)((local_c * 7) % 0x20)) =
        *(undefined *)((long)&local_38 + (long)local_c);
}
```

Vi rydder litt ved å rename "local_68" til "flag" og "local_38" til "secret" og "local_c" til "i", og renamer type/casting:
```c
for (i = 0; i < 0x20; i = i + 1) {
    *(char *)(&flag+((i * 7) % 0x20)) = *(char *)(&secret+i)
}
```

Vi ser at den setter der flagg peker pluss `(i*7)%0x20` til der secret pluss i peker. Vi kan skrive om til array notasjon og får at `flag[i*7 % 0x20] = secret[i]`. Algoritmen har stokket om på flagget, og vi kan loope hele lengden av secret og sette ting tilbake til riktig posisjon:
```python
secret = 'hfkcnsN_V}tn7_le1_kc3aRe{eh0e_pa'
flag = [""]*len(secret)
for i in range(len(secret)):
    flag[(i*7)%0x20] = secret[i]
print(flag)
# ['h', 'e', 'l', 's', 'e', 'c', 't', 'f', '{', 'e', 'N', '_', '3', 'n', 'k', 'e', '1', '_', 'p', 'a', '7', 'c', 'h', '_', 'V', 'a', 'R', '_', 'n', '0', 'k', '}']

print("".join(flag))
# helsectf{eN_3nke1_pa7ch_VaR_n0k}
```

