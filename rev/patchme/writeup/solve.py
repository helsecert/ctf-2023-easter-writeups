secret = 'hfkcnsN_V}tn7_le1_kc3aRe{eh0e_pa'
flag = [""]*len(secret)
for i in range(len(secret)):
    flag[(i*7)%0x20] = secret[i]
print(flag)
# ['h', 'e', 'l', 's', 'e', 'c', 't', 'f', '{', 'e', 'N', '_', '3', 'n', 'k', 'e', '1', '_', 'p', 'a', '7', 'c', 'h', '_', 'V', 'a', 'R', '_', 'n', '0', 'k', '}']

print("".join(flag))
# helsectf{eN_3nke1_pa7ch_VaR_n0k}
