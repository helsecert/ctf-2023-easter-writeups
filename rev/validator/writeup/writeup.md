Vi kan se på den i Ghidra:
```c

undefined8 main(void)

{
  int iVar1;
  undefined local_28 [32];
  
  printf("Skriv inn flagget: ");
  fread(local_28,0x1f,1,stdin);
  iVar1 = valid(local_28);
  if (iVar1 == 0) {
    puts("Nei, det var ikke riktig.");
  }
  else {
    printf("Gratulerer, du vet flagget! :-)");
  }
  return 0;
}

undefined8 valid(long param_1)

{
  undefined8 local_38;
  undefined8 local_30;
  undefined8 local_28;
  undefined4 local_20;
  undefined2 local_1c;
  int local_c;
  
  local_38 = 0x584d5241515a4448;
  local_30 = 0x6e686c466e4e5264;
  local_28 = 0x6e746b685269736d;
  local_20 = 0x75516852;
  local_1c = 0x6671;
  local_c = 0;
  while( true ) {
    if (0x1d < local_c) {
      return 1;
    }
    if ((char)(*(byte *)(param_1 + (long)local_c + 1) ^ *(byte *)(param_1 + local_c)) + 0x3b !=
        (int)*(char *)((long)&local_38 + (long)local_c)) break;
    local_c = local_c + 1;
  }
  return 0;
}

```

Vi kan oppgi flagget, og hvis det er riktig får vi vite det. Vi må reversere valid funksjonen for å forstå hva vi må sende inn. 

Der ligger det en streng (local_38) = 48445a5141524d5864524e6e466c686e6d736952686b746e5268517571664e = "HDZQARMXdRNnFlhnmsiRhktnRhQuqfN"

Vi ser at `(param_1[local_c] ^ param_1[local_c+1]) + 0x3b` må være lik `local_38[local_c]`. Vi kan skrive om variablene slik at det blir mer lesbart:

 (flag[i] ^ flag[i+1]) + 0x3b = secret[i]

Den sjekker byte for byte, og du får feil om noe ikke stemmer. Vi vet starten på flagget med flaggformat "helsectf{". Vi har derfor kun en ukjent i likningen over: `flag[i]`. Vi kan sette flag[0] = "h" også kjøre en algoritme som regner ut neste flag[i+1] ved å løse ut likningen over for `flag[i+1]`:

  flag[i+1] = (secret[i]-0x3b)^flag[i]

solver:
"""python
ct = "HDZQARMXdRNnFlhnmsiRhktnRhQuqfN"
flag = "h"
for key in ct:
    flag += chr((ord(key)-59)^ord(flag[-1]))
    print(flag)
"""

he
hel
hels
helse
helsec
helsect
helsectf
helsectf{
helsectf{R
helsectf{RE
helsectf{REV
helsectf{REVe
helsectf{REVen
helsectf{REVen_
helsectf{REVen_r
helsectf{REVen_rA
helsectf{REVen_rAs
helsectf{REVen_rAsK
helsectf{REVen_rAsKe
helsectf{REVen_rAsKer
helsectf{REVen_rAsKer_
helsectf{REVen_rAsKer_o
helsectf{REVen_rAsKer_oV
helsectf{REVen_rAsKer_oVe
helsectf{REVen_rAsKer_oVer
helsectf{REVen_rAsKer_oVer_
helsectf{REVen_rAsKer_oVer_I
helsectf{REVen_rAsKer_oVer_Is
helsectf{REVen_rAsKer_oVer_IsE
helsectf{REVen_rAsKer_oVer_IsEn
helsectf{REVen_rAsKer_oVer_IsEn}
