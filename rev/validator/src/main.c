#include <string.h>
#include <stdio.h>
#include <stdint.h>
#define MAX_LEN 31
int valid(char *flag) {
    char key[31] = "HDZQARMXdRNnFlhnmsiRhktnRhQuqfN";
    for (int i=0; i<30;i++) {
        if((59 + (flag[i]^flag[i+1])) != key[i]) {
            return 0 ;
        }
    }
    return 1;
}
int main()
{
    printf("Skriv inn flagget: ");
    char flag[MAX_LEN];
    fread(&flag, sizeof flag, 1, stdin);
    if(valid(flag)) {
        printf("Gratulerer, du vet flagget! :-)");
    } else {
        puts("Nei, det var ikke riktig.");
    }
	return 0;
}