import numpy as np

data = open("flag.txt", "rb").read().strip() + b"\n\x00" + \
    b"ja n@ er du p@ riktig vei!" + b"\n\x00" + \
    b"Hello, world!\n" + b"\x00" + \
    b"Du er p@ feil vei, pr0v den andre veien." + b"\n\x00"
assert len(data) < 256
memory = np.random.randint(0x20, 0x7f, 256)
for i in range(len(data)):
    memory[i] = data[i]

ip = 0
dp = data.find(b"Hello, world!")


def incdp():
    global dp
    dp += 1
    dp %= 0xff


def decdp():
    global dp
    dp -= 1
    dp %= 0xff


def output():
    print(chr(memory[dp]), end="")


intruksjoner = {
    "i": incdp,
    "d": decdp,
    "o": output,
}


def run(kode):
    global ip,dp
    ip = 0 
    dp = data.find(b"Hello, world!")
    if len(kode) > 180:
        print("Vi greier ikke å tolke programmer som er så store!")
        exit(0)        
    while ip < len(kode):
        symbol = kode[ip]
        ip += 1
        inst = intruksjoner[symbol]
        inst()

# tester
hello_world_code = "oioioioioioioioioioioioioiodddddddddddddoioioioioioioioioioioioioio"
"""
run("oi"*90)
run(hello_world_code)
solve_code = "d"*61 + "oi"*33
run(solve_code)
exit(0)
"""

def main():
    print("Vi har laget et grensesnitt for å printe data fra minne. Vårt minne er ikke så stort :(")
    print("Flagget ligger lagret i minne, og for å komme igang kan du printe Hello, world! 2 ganger med følende program:")
    print()
    print("   ", hello_world_code)
    print()
    print("Gyldige instruksjoner er:", " ".join(intruksjoner.keys()))
    try:
        kode = str(input("kode> "))
        run(kode)
    except KeyError as e:
        print("Du har brukt en ugydlig instruksjon.", e)
    except Exception as e:
        raise
        print("Noe gikk galt.")
    exit(0)

if __name__ == "__main__":
    main()
