Oppgaven handler om å forstå logikken mer enn ren reversing.

Vi blir presentert med et grensesnitt med kun 3 instruksjoner:
``` 
Vi har laget et grensesnitt for å printe data fra minne. Vårt minne er ikke så stort :(
Flagget ligger lagret i minne, og for å komme igang kan du printe Hello, world! 2 ganger med følende program:

    oioioioioioioioioioioioioiodddddddddddddoioioioioioioioioioioioioio

Gyldige instruksjoner er: i d o
kode> 

```

Vi får et eksempel vi kan kjøre:
```
kode> oioioioioioioioioioioioioiodddddddddddddoioioioioioioioioioioioioio
Hello, world!
Hello, world!
```

Hvis vi ser på koden så har den 3 biter som gir oss samme tekststreng ut 2 ganger.
```
oioioioioioioioioioioioioio
ddddddddddddd
oioioioioioioioioioioioioio
```

valgene for instruksjonene var (i)nkrementere, (d)ekrementere og (o)output. det som skjer er at vi kjører en sekvens med output fra minne, inkrementer en peker, output fra minne, inkrementer en peker osv.
så kommer en serie med 13 `d`'er og `Hello, world!` har 3 tegn. Så vi flytter pekeren tilbake 13 plasser. Og kjører samme kode på nytt.

Med det grunnlaget kan vi se hva som skjer hvis vi f.eks. fjerner d'ene, evt mater på mer flere "oioi"
```
kode> oioioioioioioioioioioioioiooioioioioioioioioioioioioio
Hello, world!

Du er p@ fei
kode> oioioioioioioioioioioioioiooioioioioioioioioioioioioiooioioioioioioioioioioioioiooioioioioioioioioioioioioiooioioioioioioioioioioioioiooioioioioioioioioioioioioio
Hello, world!

Du er p@ feiil vei, pr0v dden andre veieen.
/YgIe-|Z^^ ?Pi_a8M 5K3f
```

Her er det et tydelig hint om at vi må lese minnet ved å dekrementere først.
```
kode> dddddddddddddddddddddddddddddddddddddddddddddddddddddddoioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioioi
helsectf{inc_dec_output!}
ja n@ er du p@ riktig vei!
Hello
```
