"""
En kubisk ligning er en matematisk ligning av tredje grad. Den skrives ofte som:

  a*x^3 + b*x^2 + c*x^1 + d*x^0 = 0
  a*x^3 + b*x^2 + c*x   + d     = 0

Oppgaven gir oss

  a*m^3 + b*m + c = ct (mod p)
  
I oppgaven brukes ikke leddet for andre grad og det brukes modulær aritmetikk. Vi ønsker å løse ut ligningen for å finne `m` ved hjelp av rotutdragning. Vi kan få flere røtter, og vi forventer at flagget vil være en av de.

Vi flytter over `ct` slik at ligningen blir lik 0:

  a*m^3 + b*m + c - ct = 0 (mod p)

Nå kan vi løse ligningen i et mattematisk rammeverk som f.eks. SageMath for å finne røttene.

Bruker her Sagemath: https://www.sagemath.org/ -- fint å bruke på litt tyngre matteoppgaver.
"""
from Crypto.Util.number import *
exec(open("../src/output.txt").read())
R.<m> = PolynomialRing(GF(p))
roots = (a*m^3 + b*m + c - ct).roots()
for i,r in enumerate(roots):
    print([long_to_bytes(int(r[0]))])
