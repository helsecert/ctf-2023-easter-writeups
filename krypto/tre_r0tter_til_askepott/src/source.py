from Crypto.Util.number import *
from flag import FLAG
m = bytes_to_long(FLAG)
p = getPrime(512)
a = getPrime(512)
b = getPrime(512)
c = getPrime(512)
ct = int(a*m**3 + b*m + c)%p
print("a=",a)
print("b=",b)
print("c=",c)
print("p=",p)
print("ct=",ct)