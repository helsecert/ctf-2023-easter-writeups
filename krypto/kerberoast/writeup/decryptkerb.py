#
#
#https://cwiki.apache.org/confluence/display/DIRxSRVx10/Kerberos+ASN.1+codec

import sys
import hashlib
import hmac
from Crypto.Cipher import ARC4

try:
    checksum = bytes.fromhex(sys.argv[1])
    data = bytes.fromhex(sys.argv[2])
    password = sys.argv[3]
except Exception as e:
    print("Usage: python kerbdecode.py hexchecksum hexdata mypassword")
    exit(0)

ntlm_hash = hashlib.new('md4', password.encode('utf-16le'))
key_usage = bytes.fromhex('02000000')
key2 = hmac.new(ntlm_hash.digest(), key_usage, 'md5')
key3 = hmac.new(key2.digest(), checksum, 'md5')
decrypted = ARC4.new(key3.digest()).decrypt(data)

#print(ntlm_hash.hexdigest())
#print(key2.hexdigest())
#print(key3.hexdigest())
sys.stdout.buffer.write(decrypted)
