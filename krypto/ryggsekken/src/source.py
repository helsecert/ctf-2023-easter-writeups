from Crypto.Util.number import isPrime, getPrime,GCD, bytes_to_long
import random
FLAG = b"helsectf{?????????????????????????????????????????????????}"

n = 8
W = []
p = 1
while len(W) < n:
    if isPrime(p) and p > sum(W):        
        W.append(p)
        p = 2*p
    p+=1
q = getPrime(int(sum(W)).bit_length()+1)    
r = random.randint(2,q-1) # 1 < r < q
assert q > sum(W)
assert GCD(r,q) == 1
assert 1 < r < q

B = []
for i in range(n):    
    B.append((r*W[i])%q)

def encrypt(b):
    c = 0
    for i, bit in enumerate(bin(b)[2:].rjust(8, "0")):
        c += int(bit)*B[i]
    return c

# private_key (W,q,r)
print("public_key=", B)
print("ciphertext=",[encrypt(f) for f in FLAG])