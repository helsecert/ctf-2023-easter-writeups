"""
Dette er et Merkle-Hellman knapsack cryptosystem: https://en.wikipedia.org/wiki/Merkle%E2%80%93Hellman_knapsack_cryptosystem

Problemet er at vi bruker krypteringen feil: vi krypterer hver byte individuelt.

Dette gjør oss sårbar for å knekke chifferteksten ved at vi lager en mapping mellom en klartekst byte `f` og ciffertekst byte `c`. Vi har redusert kryptosystemet til et monoalfabetisk substitusjonschiffer.

Vi kan nå slå opp hver ciffertekstbyte for å finne riktig klartekst byte.
"""
def encrypt(b):
    c = 0
    for i, bit in enumerate(bin(b)[2:].rjust(8, "0")):
        c += int(bit)*B[i]
    return c  

exec(open("../src/output.txt").read())
B = public_key
table = { encrypt(b):b for b in range(0x20, 0x7f)}
print(table)    
print("".join([chr(table[ct]) for ct in ciphertext]))

