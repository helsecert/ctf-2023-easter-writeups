"""
Når vi bruker struct.pack('<I') på et tall så betyr `<` little-endian og `I` betyr `unsigned int`. Vi trenger bare å kjøre `struct.unpack('<I',c)` for å få ut `0x1F900 + f + 77`. Så trekker vi fra `0x1F900` og `77` og sitter igjen med flagg byten `f`. 

Hvis man sjekker alle tegn 0x20..0x7f så vil man oppdage at det er 1 tegn vi ikke kan kodes til en gylid emoji. Prøv å encode tegnet `,`:
```
>>> import struct
>>> f = ord(",")
>>> print(struct.pack('<I', 0x1F900 + f + 77).decode("utf-32le"))
🥹
```
Derfor ble tallet `77` vilkårlig funnet for å sørge for at alle tegnene vi har i flagget faktisk gir en emoji.

Vi kan derfor se på oppgaven som et monoalfabetisk substitusjonschiffer hvor 1 ASCII tegn blir byttet ut med en og samme emoji, hver gang. Vi lager en tabell for å mappe emojis til ASCII tegn.
"""
import struct
ct = "🦵🦲🦹🧀🦲🦰🧁🦳🧈🦲🦺🥽🦷🥾🧀🦬🦯🦀🦴🧆🦛🦻🦲🦿🦬🦮🦮🦬🦯🦙🦶🦬🦼🦝🦽🦯🦿🦢🦸🧁🦬🦠🥽🦺🦬🦐🦡🦓🦬🦼🦽🦝🦴🦮🧃🦀🥮🧊"

# alt1
print("".join([chr(struct.unpack('<I',c.encode('utf-32le'))[0]-0x1F900-77) for c in ct]))

# alt2
a = "".join([struct.pack('<I', 0x1F900 + f + 77).decode("utf-32le") for f in range(0x20,0x7f)])
b = "".join([chr(f) for f in range(0x20,0x7f)])
table = "".maketrans(a,b)
print(ct.translate(table))
