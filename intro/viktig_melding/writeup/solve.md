Vi ser at meldingen er kryptert med et enkelt monoalfabetisk substitusjonschiffer og at det nok antagelig kun er bokstaver + evt tall som er byttet ut:

  khovhfwi{ylnwlj_phoglqj!_vqduw_hu_ghw_såvnh!}
  helsectf{                                   }

Her ser vi at h->k, e->h, l->o, s-> v etc. Dette er Cæsarchiffer hvor alle bokstaver blir flyttet et likt antall hopp, her 3 hopp. Vi kan derfor flytte bokstavene tilbake:

Enkleste og raskeste er å ta dette med et verktøy:
- https://gchq.github.io/CyberChef/#recipe=ROT13(true,true,false,-3)&input=a2hvdmhmd2l7eWxud2xqX3Bob2dscWohX3ZxZHV3X2h1X2dod19z5XZuaCF9
- https://www.dcode.fr/caesar-cipher
