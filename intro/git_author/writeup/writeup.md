Det ligger en egen epost adresse i en av commitene i repoet. Dette kan man se hvis man bruker f.eks. `git log`:

```
commit e6c4452a1d6a857177fea98d4fbf991ade07dbba (HEAD -> main, origin/main, origin/HEAD)
Author: pAAskeharen <128143757+pAAskeharen@users.noreply.github.com>
Date:   Mon Mar 20 08:34:29 2023 +0100

    Oppdaterte README

commit 857c64a3d78b23f2cc0433644ae18372715af450
Author: pAAskeharen <68656c73656374667b6731742e656d41496c7d@pAAskeharen.kyllingland.egg>
Date:   Mon Mar 20 08:06:27 2023 +0100

    håper alle får en god påske

commit 4c160424291e26a945394ae0868793b07898af4e
Author: pAAskeharen <128143757+pAAskeharen@users.noreply.github.com>
Date:   Fri Mar 17 12:03:10 2023 +0100

    Update README.md
    
    Jeg lagrer flagget, for å være sikker.

commit 754302b6b2ed4e56b7f355c8d98e9dbaac8d3917
Author: pAAskeharen <128143757+pAAskeharen@users.noreply.github.com>
Date:   Fri Mar 17 12:01:17 2023 +0100

    Initial commit

```

Det vi er ute etter er: `Author: pAAskeharen <68656c73656374667b6731742e656d41496c7d@pAAskeharen.kyllingland.egg>`

NB! Husk at hex strengen må konvertes til bytes/string for å se flagget. Mange forsøkte å levere deler eller hele strengen over som flagg. Alle flagg følger flaggformat.

```python
>>> bytes.fromhex("68656c73656374667b6731742e656d41496c7d").decode()
'helsectf{g1t.emAIl}'
```

# alt2
Det er også mulig å se rå git commit fra web. Sleng på en ".patch" og man ser samme Author.
```
https://github.com/pAAskeharen/git/commit/857c64a3d78b23f2cc0433644ae18372715af450.patch

From 857c64a3d78b23f2cc0433644ae18372715af450 Mon Sep 17 00:00:00 2001
From: pAAskeharen
 <68656c73656374667b6731742e656d41496c7d@pAAskeharen.kyllingland.egg>
Date: Mon, 20 Mar 2023 08:06:27 +0100
Subject: [PATCH] =?UTF-8?q?h=C3=A5per=20alle=20f=C3=A5r=20en=20god=20p?=
 =?UTF-8?q?=C3=A5ske?=
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

---
 README.md | 2 ++
 1 file changed, 2 insertions(+)

diff --git a/README.md b/README.md
index a2e9e8d..0b5ec52 100644
--- a/README.md
+++ b/README.md
@@ -1,3 +1,5 @@
 # git
 
 helsectf{git_er_g0dt!}
+
+god påske! :-)


```