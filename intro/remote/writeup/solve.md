"gratis" flagg.

Klipp og lim det som står i oppgaven i et python console og man får ut flagget:

```python
>>> 
>>> from pwn import *
>>> io = remote("helsectf2023-6ac4e1c6d8855c1bd96a-remote.chals.io", 443, ssl=True)  
[x] Opening connection to helsectf2023-6ac4e1c6d8855c1bd96a-remote.chals.io on port 443
[x] Opening connection to helsectf2023-6ac4e1c6d8855c1bd96a-remote.chals.io on port 443: Trying 143.244.222.116
[+] Opening connection to helsectf2023-6ac4e1c6d8855c1bd96a-remote.chals.io on port 443: Done
>>> print(io.recvline().decode()) 
Her har du et flagg: helsectf{remote_flag_killer}

>>> 
```