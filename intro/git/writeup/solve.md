Meldingene har Påskeharen skrevet som "pAAskeharen". Dette var for å gi et hint om kontonavnet på github.com.

Det man skal finne seg frem til er repoet:
  https://github.com/pAAskeharen/git/

Her ligger flagget i README.md og er godt synlig når man åpner URLen.