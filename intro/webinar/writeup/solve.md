Søking gir oss Webinar på nhn.no sine nettsider:
  https://www.nhn.no/om-oss/Personvern-og-informasjonssikkerhet/helsecert/webinarer
  
Siste webinar (p.t.) er Windows-logging. https://vimeo.com/806667945/644a590a20

fra 2:15min og 1min30s så vil man se flagget nede til høyre på powerpointen. Den kan være vanskelg å se fordi den ligger bak tidsvelgeren hvis man har muspekeren inne/over i videoen. De ivrigste som derfor har hopper frem og tilbake
i webinaret ville nok ikke sett dette med en gang.

Det står i oppgaven: du må se webinaret for å finne flagget

helsectf{webinar_er_g0y}