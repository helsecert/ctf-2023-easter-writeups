Vi kan laste ned originalfilen og se hva som er endret:

```bash
$ diff <(xxd Årsrapport\ 2021\ Norsk\ helsenett.pdf) <(xxd Årsrapport_2021_Norsk_Helsenett.pdf)
420082c420082,420085
< 00668f10: 460d 0a                                  F..
---
> 00668f10: 460d 0a68 656c 7365 6374 667b 6865 6c64  F..helsectf{held
> 00668f20: 6967 7669 735f 7661 725f 696b 6b65 5f64  igvis_var_ikke_d
> 00668f30: 6574 7465 5f6f 6e64 7369 6e6e 6574 5f6b  ette_ondsinnet_k
> 00668f40: 6f64 657d                                ode}

```

Evt bare "strings <fil> | grep helsectf"

Flagget ligger lagt til slutten av filen.
