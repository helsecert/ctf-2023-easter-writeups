I git vil man finne repoet "smartkontrakter" med en QRkode. QRkoden har setninger fra shakespeare. Om man sjekker git commit så vil man
se at qrkoden er overskrevet mange ganger. Vi henter ut alle qrkodene og leser hva de inneholder.

Først dumper vi alle git commit hashene: `git log | grep "^commit" | cut -d" " -f2 > hashes.txt`

Så kjører vi et script for å hente ut filen som er for hver commit:
```python
import os
from PIL import Image
from pyzbar.pyzbar import decode
hashes = open("hashes.txt").readlines()
for i,h in enumerate(hashes):
    print(h.strip())
    os.system(f"git show {h.strip()}:qrcode.png > qrcodes/{i}.png")    
    img = Image.open(f'qrcodes/{i}.png')    
    data = decode(img)
    text = data[0][0]
    print(text)
```

```
But as the riper should by time decease,
Nature's bequest gives nothing but doth lend,
And dig deep trenches in thy beauty's field,
Which bounteous gift thou shouldst in bounty cherish:
Jeg har lekt med blokkjeder.
For 疇 l疆re har jeg kun holdt meg til et ETH testnett.
Haaper jeg ikke har gjort noen feil!
helsectf{g1t_bEhOlD3r_h1sTor1KkeN!}                             <--- solve for git/git huskelapp
etherum:0xb53449a6cd4D55bD09fD3f4509307139c354cc18              <--- starten på osint/welcome
Upon thy self thy beauty's legacy?
No longer yours, than you your self here live,
That thou no form of thee hast left behind,
```
