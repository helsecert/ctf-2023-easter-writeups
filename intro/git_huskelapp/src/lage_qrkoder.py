import qrcode
import random
noise = open("shakespeare.txt","r")
texts = ["""Jeg har lekt med blokkjeder.
For å lære har jeg kun holdt meg til et ETH testnett.
Haaper jeg ikke har gjort noen feil!
helsectf{g1t_bEhOlD3r_h1sTor1KkeN!}
etherum:0xb53449a6cd4D55bD09fD3f4509307139c354cc18"""]
for line in noise.readlines():
    line = line.strip()
    if len(line) < 1:
        continue
    texts.append(line)

texts = texts[0:200] # 200 er kanskje nok :D

random.shuffle(texts)
for i,t in enumerate(texts):
    if "etherum" in t:
        print(i,t)
    img = qrcode.make(t)
    type(img)# qrcode.image.pil.PilImage
    img.save(f"qrcodes/{i}.png")

