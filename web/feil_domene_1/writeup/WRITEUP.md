Vi sjekker hvilket domenet sertifikatet på webserveren er utstedt for:

```$ echo Q | openssl s_client -connect 161.35.192.95:443 | grep CN```

Vi besøker serveren igjen med riktig Host-header:

```curl -i http://161.35.192.95/ -H 'Host: seretdomain-88a.helsectf.no'```

Voila!
