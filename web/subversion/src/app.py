# app.py
from flask import Flask

app = Flask(__name__,
            static_url_path='/.svn', 
            static_folder='static')

@app.route("/")
def default():
    return "\n"
