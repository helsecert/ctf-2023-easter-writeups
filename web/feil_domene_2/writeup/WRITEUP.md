Vi bruker https://crt.sh/ for å søke i certstream-data og finner at 23. mars 2020 ble det utstedt et sertifikat til xn--pskentter-52a7s.helsectf.no

Vi besøker serveren igjen med riktig Host-header:

```curl -k -i https://161.35.192.95/ -H 'Host: xn--pskentter-52a7s.helsectf.no'```

Voila!
