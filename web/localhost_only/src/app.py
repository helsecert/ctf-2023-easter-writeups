# app.py
from flask import Flask
from flask import request


app = Flask(__name__)


@app.route("/")
def default():
   return "You should access the admin portal!"


@app.route("/admin/")
def admin():
    if 'x-forwarded-for' in request.headers and "127.0.0.1" in request.headers['x-forwarded-for']:
        return "helsectf{x-forwarded-4ever<3}"
    else:
        return "<p>This portal is only accessible from localhost</p>"
