# Tode

Tode er en videreføring av oppgave Ende. Oppgavenavnet hinter til at vi må se til to dimensjoner for å finne den skjulte teksten. 

Analyserer vi pcapen på samme måte som i oppave Ende finner vi 7 bilder med bokstaver og tegn i alfa kanal 0. Løsningen er å stacke bildene - altså legge dem over hverandre. 

I python kan dette se slik ut:

```
from PIL import Image

width = 1024 
height = 1024
opaque = 255
hiddenPixel = 254

def solve2():   
   result = Image.new(mode="RGBA", size = (width, height), color=(255, 255, 255, 255))
    pixels = list(result.getdata())
    
    for x in range(7):
        img = Image.open(f"oppgave2_{x}.png")
        imgPixels = list(img.getdata())
        for p in range(len(imgPixels)):
            pixels[p] = (0,0,0, opaque if imgPixels[p][3] == hiddenPixel else pixels[p][3]) 
    
    result.putdata(pixels)
    result.save("oppgave2_solve.png")
```

![Løsning Tode](./oppgave2.png "Løsning 2D")
