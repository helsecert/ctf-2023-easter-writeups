Gåten forteller at man må lete etter det største bildet, og at noe gjemmer seg i dette. Dette er rue.jpg.
1. Bruk et verktøy som `steghide` til å hente ut gjemt informasjon fra bildet.
- Eksempel: `steghide extract -sf rue.jpg` (uten passord)
- Man får ut `ameliore.jpg`. Dette bildet virker som det er veldig komprimert.
2. Bruk et verktøy som `exiftool` til å hente ut metadata til bildet.
- Eksempel: `exiftool ameliore.jpg`
- Man får ut `Author: u/LeChatCurieux93`. Dette er en referanse til en redditkonto.
3. Google `u/LeChatCurieux93`. Man finner da: https://www.reddit.com/user/LeChatCurieux93/
- I profilbeskrivelsen står følgende: Le meilleur moment de ma vie: leprintemps2019!
- Dette er passordet man trenger til å hente ut den skjulte filen i ameliore.jpg.
4. Bruk et verktøy som `steghide` til å hente ut gjemt informasjon fra bildet.
- Eksempel: `steghide extract -sf ameliore.jpg -p leprintemps2019!`
- Man får ut `ameliore.orig`, som er en tekstfil med innholdet: aHR0cHM6Ly9tZWdhLm56L2ZpbGUvaXJnVm5RclQjTEZVRHcycV94U1dsRVhvZGRjOEpob0w0Y0lEUFZJRHlTaEdTVXpnSGx5aw==
5. Konverter Base64-strengen. Man får da: https://mega.nz/file/irgVnQrT#LFUDw2q_xSWlEXoddc8JhoL4cIDPVIDyShGSUzgHlyk
6. Last ned bildet fra lenken. Dette er originalbildet `ameliore.jpg`.
7. Zoom inn på sykkelskiltet i bildet. Her står flagget i klartekst.

Oppgavetittelen er en referanse til Inception.
