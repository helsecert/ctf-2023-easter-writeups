# Ende

Kategorinavnet og oppgaveteksten hinter til at dette er en steganografi oppgave. Vi er altså på jakt etter bilder med skjult innhold.

Inspeksjon og uthenting av data fra en pcap kan gjøres på mange måter og med
mange verktøy og Wireshark er en av disse. 

Du vil da se at dette er en enkel http sessjon med en post og en respons og 
at post requesten er en multi-form med et PNG bilde.

Du kan eksportere bildet med å markere bildet og så velge Export Packet Bytes i kontekst menyen. 

![Hente ut PNG fra PCAP](./wireshark.png "Wireshark PNG Eksport")

Bildet er nå klar til analyse. https://stegonline.georgeom.net er 
et godt startsted for slikt.

![Online løsning](./stegonline.png "Løsning med SteoOnline")

Med litt prøving og feiling vil man finne ut at et bit-plan 0 i 
alphakanalen på bildet er brukt til å skrive data. Dvs noen av pixelene i bildet har en alfa verdi på 254 isteden for 255. Hvis man kun ser på disse pixelene vil man se flagget.


## Side spor og morsomheter
* Beklager til alle som brukte mye til å dekode trønderske uttrykk i responsen. 
* At apiet bildet ble sendt til var til /api/put/in kunne kanskje tyde på russiske forbinnelser?

## Python
Gitt at man vet at bildet ligger i alpha 0 kanalen kan man bruke følgende
python til å få ut bildet. 


```
from PIL import Image

# Image size
width = 1024 
height = 1024

# Some alpha values
transparent = 0 
opaque = 255 
hiddenBitAlpha = 254

def solve1():
    img = Image.open("oppgave1.png")
    imgPixels = list(img.getdata())
    nofPixels = len(imgPixels)

    resultData = []
    for x in range(nofPixels):
        imgPixel = imgPixels[x] 
        resultData.append((0,0,0, opaque if imgPixel[3] == hiddenBitAlpha else transparent))
    
    
    result = Image.new(mode="RGBA", size = (width, height), color=(255, 255, 255, transparent))
    result.putdata(resultData)
    result.save("oppgave1_solve.png")

```

