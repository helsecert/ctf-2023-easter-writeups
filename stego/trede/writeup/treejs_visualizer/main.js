import './style.css';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer({
  canvas: document.querySelector('#bg'),
});

renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.render(scene, camera);

function addImage(path, z) { 
  var imgLoader = new THREE.TextureLoader();
  var imgMaterial = new THREE.MeshLambertMaterial({
    transparent: true,
    map: imgLoader.load(`./images/${path}`)
  });
var imgGeometry = new THREE.PlaneGeometry(100, 100);
  var imgMesh = new THREE.Mesh(imgGeometry, imgMaterial);
  imgMesh.position.set(0,0,z)
  scene.add(imgMesh)
}

function task3_txt() {
 addImage('task3_txt_0.png', 0);
 addImage('task3_txt_1.png', 25);
 addImage('task3_txt_2.png', 50);
 addImage('task3_txt_3.png', 75);
 addImage('task3_txt_4.png', 100);
}

task3_txt(); 

const pointLight = new THREE.PointLight(0xffffff);
pointLight.position.set(5, 5, 5);

const ambientLight = new THREE.AmbientLight(0xffffff);
scene.add(pointLight, ambientLight);

const controls = new OrbitControls(camera, renderer.domElement);

const spaceTexture = new THREE.TextureLoader().load('space.jpg');
scene.background = spaceTexture;

function moveCamera() {
  const t = document.body.getBoundingClientRect().top;
}

document.body.onscroll = moveCamera;

camera.lookAt(50,50,20)
camera.position.setZ(150);
camera.position.setX(-80);
camera.position.setY(20);
moveCamera();
controls.update()

function animate() {
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
}

animate();
