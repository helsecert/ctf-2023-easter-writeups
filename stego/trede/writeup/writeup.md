# Trede

Oppgavenavnet Trede hinter til at vi må bruke tre dimensjoner for å finne løsningen. På samme måte som oppgave Ende og Tode henter vi ut alle bildene fra pcap og drar ut alfa kanal 0 i egne bilder.

Oppgaven kan løses med å forskyve bildene horisontalt i forhold tilhverandre men flagget vil da oppleves som litt usammenhengende. 

Den ideelle løsningen er å sette bildene opp i en kube - dvs at lengde, bredde og dypde i bildene er like. Rekkefølgen på bildene ble gitt i som en "z" parameter i post requesten. Ved å rotere denne kuben vil man tilslutt se at bokstavene som danner flagget vil komme sammen.

![Tredimensjonal Løsning](./trede.gif "Løsning i 3D")

Koden for denne visualiseringen ligger i treejs_visualizer.
