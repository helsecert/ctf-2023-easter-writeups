/*
# JA3
Oppgaven handler om JA3 Fingerprinting. Det går i korte trekk ut på at man kan danne et md5hash av 5 felter fra SSLClientHello pakken som en klient applikasjon sender til en TLS server i oppkoblingsfasen. Dette er før krypteringen er etablert, og dette har da fungert godt opp til TLS versjon 1.2. Formålet er å fingerprinte applikasjonen som kobler til. Det fungerer kun hvis fingeravtrykket er unikt. Et stort problem er kollisjoner; to ulike applikasjoner kan sende samme feltene i samme rekkefølge i SSLClientHello og man vil da få samme fingeravtrykk.

Da JA3 kom var det knallbra! Men som med alt så taper det verdi over tid. Ett av de største problemene var at folk fant ut at de kunne unngå JA3 fingerprinting ved å randomisere rekkefølgen på innholdet i feltene.

Feltene som tas ut er in-order, og bygger ja3_strengen: SSLVersion,Cipher,SSLExtension,EllipticCurve,EllipticCurvePointFormat og JA3 hashet er en md5sum av dette hvor feltene joines sammen med - (dash).

# Oppgaven
Du må koble til serveren med riktig innhold i disse feltene, i riktig rekkefølge, for at du skal få riktig JA3 hash -- og et flagg. Det du skal gjør er å sende identisk innhold som malwaren Emotet bruker.

I oppgaven oppgis Emotet og Emotet har et kjent JA3 fingerprint: 4d7a28d6f2263ed61de88ca66eb011e3 -- her må vi bruke åpne kilder for å vite hvilken ja3_streng som gir Emotet sitt hash (det er bare en md5sum av ja3_strengen med innhold separert med komma). Det er flere kilder:
- https://github.com/salesforce/ja3/issues/41
- https://github.com/trisulnsm/trisul-scripts/blob/master/lua/frontend_scripts/reassembly/ja3/prints/ja3fingerprint.json

Disse gir oss en ja3_str="771,60-47-61-53-5-10-49191-49171-49172-49195-49187-49196-49188-49161-49162-64-50-106-56-19-4,65281-0-10-11-13,23-24,0"

For å vise hvordan JA3 hashet laget så kan vi nå ta en md5sum av dette og vi får fingeravtrykket. Dette er det som skjer i verktøy som leser PCAP filer eller passiv nettverkstrafikk. De henter ut feltene fra SSLClientHello, bygger ja3_strengen og lager md5sum:
```
$ echo -n "771,60-47-61-53-5-10-49191-49171-49172-49195-49187-49196-49188-49161-49162-64-50-106-56-19-4,65281-0-10-11-13,23-24,0" | md5sum
4d7a28d6f2263ed61de88ca66eb011e3  -
```

Vi må nå bruke en TLS klient applikasjon hvor vi kan sette disse verdiene før vi gjør oppkoblingen. Husk at rekkefølgen internt for hvert felt er ekstremt viktig. Hvis vi endrer på 1 byte i den strengen så vil fingeravtrykket bli totalt forskjellig, pga md5.

Dette kan lett løses i Go(lang) med riktig bibliotek.  Alternativ er å bygge opp en TLS Handshake ClientHello med Scapy eller andre biblioteker.

Koden under gir oss flagget.

```html
<html><head><title>Et nettsted</title></head><h1>ADVARSEL! Emotet er ikke 0nsket her.</h1><br><h1>Ditt fingeravtrykk er 4d7a28d6f2263ed61de88ca66eb011e3</h1><br><h1>flag:helsectf{TLS_fingerprint_med_JA3_kan_oppdage_cyberangrep}</h1></html>
```
*/
package main

import (
	"fmt"
	"io"
	"net/http"

	"github.com/CUCyber/ja3transport"
	tls "github.com/refraction-networking/utls"
)

func main() {
	config := &tls.Config{InsecureSkipVerify: true}
	ja3_str := "771,60-47-61-53-5-10-49191-49171-49172-49195-49187-49196-49188-49161-49162-64-50-106-56-19-4,65281-0-10-11-13,23-24,0"
	tr, _ := ja3transport.NewTransportWithConfig(ja3_str, config)
	client := &http.Client{Transport: tr}
	//host := "https://localhost:8000"
	host := "https://0.cloud.chals.io:15185"
	resp, err := client.Get(host)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		b, err := io.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}
		fmt.Println(host)
		fmt.Println(string(b))
	}
}
