module main

go 1.20

require (
	github.com/CUCyber/ja3transport v0.0.0-20201031204932-8a22ac8ab5d7
	github.com/refraction-networking/utls v0.0.0-20200820030103-33a29038e742
)

require (
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897 // indirect
	golang.org/x/sys v0.0.0-20201029080932-201ba4db2418 // indirect
)
