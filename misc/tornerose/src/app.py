# app.py
from flask import Flask, request
import subprocess

app = Flask(__name__)

@app.route("/")
def default():
    program = request.args.get('program')
    if not program:
        return 'Send inn et program, f.eks: /?program=print("tornerose/prins")\n'
    elif len(program) > 80:
        return 'Vi har satt inn en maksgrense på 80 tegn, slik at programmet skal få plass på en linje.\n'
    elif not all([x in 'einoprst"/()' for x in program]):
        illegal = set()
        for ch in program:
            if ch not in 'einoprst"/()':
                illegal.add(hex(ord(ch)))
        return 'Du har ulovlige tegn i programmet ditt: ' + " ".join(illegal) + '\n'
    else:
        try:
            return subprocess.check_output(['python', '-c', program])
        except:
            return "Programmet ditt feila på et eller annet vis. Enten feil syntaks eller runtime error.\n"
