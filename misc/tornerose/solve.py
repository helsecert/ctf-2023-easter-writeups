def valid(s):
    for l in s:
        if not l in "einoprst\"/()":
            raise Exception("ugyldig symbol: %s" % l)
    return True

def chal(program):
    valid(program)
    exec(program)

chal("""print("tornerose/prins")""")
chal("""print(open("rosetorn"))""")
