from PIL import Image, ImageDraw
import os

"""
Vi ser at alle bildene har gul bakgrunn med ulik styrke.

images/18.png (238, 238, 18)
images/146.png (238, 238, 146)
images/94.png (238, 238, 94)
images/26.png (238, 238, 26)
images/142.png (238, 238, 142)
images/38.png (238, 238, 38)
images/86.png (238, 238, 86)
images/30.png (238, 238, 30)
images/14.png (238, 238, 14)
images/62.png (238, 238, 62)
images/102.png (238, 238, 102)
images/74.png (238, 238, 74)
images/90.png (238, 238, 90)
images/54.png (238, 238, 54)
images/138.png (238, 238, 138)
images/46.png (238, 238, 46)
images/134.png (238, 238, 134)
images/114.png (238, 238, 114)
images/22.png (238, 238, 22)
images/110.png (238, 238, 110)
images/130.png (238, 238, 130)
images/150.png (238, 238, 150)
images/50.png (238, 238, 50)
images/78.png (238, 238, 78)
images/42.png (238, 238, 42)
images/66.png (238, 238, 66)
images/34.png (238, 238, 34)
images/122.png (238, 238, 122)
images/126.png (238, 238, 126)
images/98.png (238, 238, 98)
images/70.png (238, 238, 70)
images/82.png (238, 238, 82)
images/118.png (238, 238, 118)
images/106.png (238, 238, 106)
images/58.png (238, 238, 58)

Vi kan rename filene til blå fargekode og åpne mappen med bilder. Nå kan vi lese ut flagget in-order.
"""

image_list = os.listdir("images/")
for i,filename in enumerate(image_list):
    fn = "images/"+filename
    im = Image.open(fn)
    pix = im.load()    
    pix = pix[0,0]
    print(fn, pix)
    os.rename(fn, "images/"+str(pix[2])+".png")