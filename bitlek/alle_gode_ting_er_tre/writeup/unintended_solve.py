"""
Han som QA'et oppgaven internt fant en unintended solve som løser flagget rett ut.

Vi lot oppgaven stå som den var.
"""
def to21bit(t):
    # 3byte blir gjort om til 21bit (fjerner MSB fra hver byte)    
    return ((t[0]&127)) << 14 | ((t[1]&127)) << 7 | ((t[2]&127))    

def from21bit(b):
    # ekspanderer 21bit tilbake til 3bytes (3x8bit) hvor MSB settes til 0 i hver byte
    return (((b>>14)&127)<<16) | (((b>>7)&127)<<8) | (b&127)

def scramble(data):
    # input bytes blir rotet sammen til et 21bits tall
    a = 0xcbf29ce484222325
    for byte in data:
        a = a ^ byte
        a = (a * 0x100000001b3) % 2**64        
    b = a&0x1fffff  
    c = (a>>21)&0x1fffff
    d = (a>>42)&0x1fffff
    return b^c^d # xor-folding for å få 21bits output

def nofBytesOf7bitNumber(bigNumber):
    b = bin(bigNumber)
    msb = len(b) - 2
    rem = msb % 7
    return int(msb / 7 + (1 if rem > 0 else 0))


def cipherTriplets(cipher):
    nof_bytes = nofBytesOf7bitNumber(cipher)
    triplets = []
    for i in range(0, nof_bytes, 3):
        cipher21bit = (cipher >> i * 7) & 0x1fffff
        triplets.append(cipher21bit)
    return triplets


cipher = 91288880675628035011093545005790682206962326526026631772248877471574034941238763064368507363758616986
triplets = cipherTriplets(cipher)

# We know that second triplet is a scramble of 'hel'
lastPlain = b"hel"
plains = []
for i in range(1, len(triplets)):
    key = scramble(lastPlain)
    plain21Int = triplets[i] ^ key
    plainBytes = int.to_bytes(from21bit(plain21Int), 3, byteorder='big')
    plains.append(plainBytes.decode('utf-8'))
    lastPlain = plainBytes

plainTextSoFar = ''.join(plains)

# Now the last bytes is the key for the first triplet
print("Last plain: {}", lastPlain)
key = scramble(lastPlain)
plain21Int = triplets[0] ^ key
plainBytes = int.to_bytes(from21bit(plain21Int), 3, byteorder='big')
print("Plains: {}".format(plainBytes.decode('utf-8') + plainTextSoFar))

