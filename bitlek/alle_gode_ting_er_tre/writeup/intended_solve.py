def to21bit(t):
    # 3byte blir gjort om til 21bit (fjerner MSB fra hver byte)    
    return ((t[0]&127)) << 14 | ((t[1]&127)) << 7 | ((t[2]&127))    

def from21bit(b):
    # ekspanderer 21bit tilbake til 3bytes (3x8bit) hvor MSB settes til 0 i hver byte
    return (((b>>14)&127)<<16) | (((b>>7)&127)<<8) | (b&127)

def scramble(data):
    # input bytes blir rotet sammen til et 21bits tall
    a = 0xcbf29ce484222325
    for byte in data:
        a = a ^ byte
        a = (a * 0x100000001b3) % 2**64        
    b = a&0x1fffff  
    c = (a>>21)&0x1fffff
    d = (a>>42)&0x1fffff
    return b^c^d # xor-folding for å få 21bits output

def encrypt4(flag):
    plaintexts = [flag[i:i+3] for i in range(0, len(flag), 3)]
    keys = [scramble(tre) for tre in plaintexts]
    keys = [keys[-1]] + keys[0:-1]
       
    ciphertext = 0
    for i,ct in enumerate([to21bit(pt)^key for pt,key in zip(plaintexts,keys)]):
        ciphertext |= (ct << (21*i))
    return ciphertext

"""
Denne oppgaven er en kombinasjon av litt bitfikling og FNV hash bruteforcing.

Flagget er delt opp i chunks på 3 bytes, totalt 16 biter -- 16*3 = 48tegns flag.
Ut fra koden ser vi at ciphertextene er resultatet av XOR mellom
en 21bit (3x7bit) ascii versjon av plaintext og 21bit resultatet av scramble(plaintext)

vi ser at scramble(pt) er rotert med -1 slik at ciphertextene ct, plaintextene pt
og det som kommer ut av scramble(pt_k) blir slik:

ct0 = to21bit(pt0)  ^ scramble(pt15)
ct1 = to21bit(pt1)  ^ scramble(pt0)
ct2 = to21bit(pt2)  ^ scramble(pt1)
..
ct14= to21bit(pt14) ^ scramble(pt13)
ct15= to21bit(pt15) ^ scramble(pt14)

Men hva gjør funksjonen scramble? Hvis vi søker etter konstanten 0xcbf29ce484222325 på google så finner vi at dette er FNV. FNV er en ikke-kryptografisk hash som er lynkjapp og produserer et tall ut fra n-antall bit. Her har man brukt 64bit versjonen og gjort _hash folding_ (XORe deler av hashet sammen for å danne et hash med færre bits) for å skape et 21bit tall. Siden det ikke er et kryptografisk hash og siden vi reduserer det til 21bit vil kollisjoner kunne forekomme, men det bør ikke mange.

Hvordan kan vi da løse den? Hasher kan normalt bruteforces, men 2^32 vil ta lang tid.

Vi vet at input til FNV er 3bytes (24bit) og vi vet at det er ASCII så MSB er satt til null i alle de 3 bytsens. Det er derfor kun 21bit ukjent. Vi kan prøve alle 2^21 kombinasjoner med FNV og det bør ikke ta mer enn 5-6 sekunder på en greit laptop. Husk at det er 3byte, 24 bit som skal til FNV, så man må konvertere 3*7=21bit tilbake til 3*8=24bit=3bytes før man prøver FNV.

De første plaintekstene er kjent, siden det starter med `helsectf{` som kan splittes opp i 3 blokker på `hel`, `sec` og `tf{`. Ved å se på føste ligning finner vi `scramble(pt15) = ct0 ^ to21bit(pt0)`. For å finne pt15 kan vi nå knekke `scamble(pt15)` ved bruteforce. Når vi nå vet pt15 så kan vi se på siste likning hvor vi har `scramble(pt14) = ct15^to21bit(pt15)`. Nå kan vi knekke for å finne pt14.

Vi gjentar dette bakover for å finne p15, p14, p13, .. pt0. En god sjekk på at vi har gjort alt riktig er at pt2 blir "tf{".

...

Men, en utfordring er at hashet har kollisjoner, så vi må sjekke flere kandidater.
Dvs gitt et hash h0 så vil det kunne være flere enn 1 input som vil gi `h0 = scramble(input)`.

Hvordan vet vi hvilken kandidat som er riktig? Det er nok mulig å løse denne manuelt per steg, og velge seg ut hvilken kandidat som "ser" mest riktig ut (flagget er lesbart på norsk). En annen måte er at en eller flere påfølgende hasher ikke vil gi noe resultat hvis en kandidat var feil. Hvis vi knekker og ikke får et resultat må vi gå tilbake og forkaste kandidate(r), og prøve en ny "avgreining".

Vi kan se på dette som en depth-first algoritme:
  vi jobber oss fra toppen av et tre (slutten av flagget) og nedover i en trestruktur hvor vi prøver ulike kandidater og går opp og tilbake i tre'et hvis en kandidat ikke viser seg å fungere.

Vi har 13 ukjente biter av plaintexten, og muligens litt backtracking på kandidater som ikke er riktige. Burde ikke ta mye mer enn 2-3 minutter med single kjerne. Med multiprosessing på 16 kjerner tar det ca 30s.

Det er kanskje like raskt å løse den manuelt for hvert steg, enn å skrive en stor kode for det.
"""

from Crypto.Util.number import long_to_bytes, bytes_to_long
from tqdm import tqdm
import string
printable_chars = bytes(string.digits + string.ascii_letters + string.punctuation, 'ascii') #"!#$%&'()*+,-./:;<=>?@[\]^_`{|}~") # "!?@+-,:|=(){}_", 'ascii')

exec(open("../src/output.txt").read())

# henter ut 21bit pt^key
ciphertexts = []
c = ct
while c > 0:
    ciphertexts.append(c & 0x1fffff)
    c = c >> 21
print(ciphertexts, len(ciphertexts))

from multiprocessing import Pool
from functools import partial

def crack(h0, i):
    b0 = long_to_bytes(from21bit(i))
    if len(b0) != 3:
        return
    h1 = scramble(b0)
    if h0 == h1 and all(ch in printable_chars for ch in b0):            
        return b0        
    return

def scramble_bruteforce_multi(h0):    
    with Pool(16) as p:
        candidates = p.map(partial(crack, h0), range(2**21))
        candidates = list(filter(None, candidates))
        if len(candidates):
            print("[+] found candidates", candidates)
        return candidates

def scramble_bruteforce_single(h0):
    candidates = []
    for i in tqdm(range(2**21)):   
        b0 = crack(h0,i)
        if b0 != None:
            print("[+] found",b0)
            candidates.append(b0)
    return candidates

def scramble_bruteforce(h0):
    return scramble_bruteforce_multi(h0)
    #return scramble_bruteforce_single(h0)

# Vi setter inn de kjente klartekstene vi vet om, hel sec tf{
print("cts", len(ciphertexts))
plaintexts = [[b"hel"]] + [[b"sec"]] + [[]]*(len(ciphertexts)-2)
fns = [0]*len(ciphertexts)
L = len(plaintexts) # antall 3byte biter
i = L-1
while i >= 0:
    candidates = plaintexts[(i+1)%L]        
    pt_ = candidates[0] # Vi velger en kandidat som skal forsøkes.
    fns[i] = to21bit(pt_)^ciphertexts[(i+1)%L] # finner scramble(pt_i)
    cand = scramble_bruteforce(fns[i]) # knekker scramble(pt_i) for å finne kandidater
    if i == 0: # cand er nå knekkingen av scramble(pt0)
        # her må vi enten ha tf{ (pt2) i candidatene, ellers er noe veldig galt
        # og vi vil gå tilbake for å prøve andre kandidater.        
        if b"tf{" in cand:
            break
        cand = []         
    print("candidates=",cand)       
    if len(cand) < 1:                
        for x in range(i+1,L):
            print(x,"backtrack")
            plaintexts[x].pop(0)
            if len(plaintexts[x])>0: # prøver andre kandidater hvis vi har det.
                i = x-1
                break                   
            # hvis vi ikke har flere kanidater,  hopper vi enda en tilbake.
            # ved neste iterasjon hvis vi fremdeles ikke har flere kandidater, hopper vi enda tilbake... helt tilbake til start.
            # vi kan se på dette som en depth-first algoritme i en trestruktur fra toppen og ned hvor toppen er siste del av flagget og bunn er starten.
        continue
    plaintexts[i] = cand
    flag = b"".join([x[0] if len(x) else b" "*3 for x in plaintexts]).decode()    
    print(i, "FLAG", flag)
    print(i, plaintexts)        
    i -= 1
    if "helsectf{" in flag: break

print("\n\n",flag)