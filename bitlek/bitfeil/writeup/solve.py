# solve
"""
Med hintet Richard, funksjonen p som teller opp 1ere (dvs paritet) og bruk av bittene 0,1,3 0,2,3 1,2,3
samt at man overfører 4 bit og får en 7bits melding hvor bitpos 1,2,4 er paritet ==> Dette er Hamming 7,4.

Richard Hamming ville ikke bare oppdage bitfeil men også gjøre feilkorrigering. På 50 tallet fant han opp en
algoritme som kan fikse enbitsfeil på en mer optimal måte enn tidligere metoder, og Hamming kodene er fremdeles
i bruk den dag i dag.

For å få oppgaven til å "gå opp" litt bedre flytter vi 7bits ASCII ned til 6bit med et "hopp". Dette slik
at 2byte 7bit ASCII blir gjort om til 3 7bits hamming7,4 via 6bit (6bit + 6bit = 12 bit // 4 bit = 3 4bits meldinger).
Dette begrenser hvilke ASCII tegn som kan være i flagget (så ASCII veriden av tegnet minus 2**6-1 = 63 ikke blir negativ).

Vi løser oppgaven ved å bruke Hamming 7,4 dekoder for å korrigering eventuelle bitfeil før vi kaller
`print_bits` på nytt. Med 75% bitfeilrate per 7bit ser vi en del av flagget før vi begynner å korrigere,
håper det gav motivasjon til å løse oppgaven -- og et relativt langt flagg.

== forenklet hamming 7,4 dekoder:

for hamming 7,4 vil encoding bruke bitposisjonene 1,2,4 (index i python 0,1,3) for paritetsbits,
mens bitpos 3,5,6,7 er data:
    p(b,[0,1,3]) + p(b,[0,2,3]) + b[0] + p(b,[1,2,3]) + b[1:]

error oppdages ved at man XORer sammen bitposisjonene som har 1bit satt. Hvis summen er >0 så
vil svaret på XOR operasjonen si hvilket bit som er feil.

! Merk at Hamming 7,4 ikke tåler at flere bits blir flippet. Da vil xor summen (feilaktig) kunne bli 0.
! Husk at bitpos er 1..7 mens vi jobber i python arrays 0..6 så vi må gjøre noen små korrigeringer (+1 -1)

En ratio med 4/7 i utnyttet båndbredde høres lavt ut (57%) -- hamming koden støtter større meldinger. antall
bit paritet avgjør størrelsen med formelen 2^(antall paritet)-1 -- i oppgaven her har vi 3 bit partitet så 
vi får 2^3 - 1 = 7 bits melding - 3 bit paritet = 4 bit data -- altså Hamming 7,4.

Hvis vi hadde f.eks. brukt 8bit paritet ville vi hatt 2^8 - 1 = 255 bits melding - 8bit paritet = 247 bit data,
dvs Hamming 255,257. Vi ser derfor at ratioen på data/total bits øker veldig, fra ca 57% til 96%.

Men (!) vi kan fremdeles kun ha 1 bit feil på hele meldingen -- jo mindre meldinger, jo flere bit feil per
overført data -- jo større meldinger jo dårligere blir antall bit feil vi kan fikse. Hamming 7,4 tåler 1bit
feil per 7bit overført data mens Hamming 255,247 tåler 1bit feil per 255bit overført data.

Hamming koden er derfor best egnet der vi har en lav error rate men hvor det er viktig at en singel en-bits feil
ikke forstyrrer/ødelegger systemet. Dette er grunnen til at man bruker Hamming koden i blant annet i ECC-minne
("Error-correcing code memory").

Så nå vet du hva "ECC" merkingen på RAM betyr neste gang du skal kjøpe deg en ny datamaskin :-)
"""
hopp = 2**6 - 1
def print_bits(bits):
    bits = "".join([bits[i+2] + bits[i+4:i+7] for i in range(0, len(bits), 7)])
    melding = [int(bits[i:i+6],2)+hopp for i in range(0, len(bits), 6)]    
    return bytes(melding).decode()
bits = "101001011001101100110011001100010111101100001100110100101001100101010101001100001111111111110000000001110001000110100100011010011101111000100111101001110011010011000011011110000101101011000010101001010101110110010110000101101110110110001101011000100101001000111110100000000100001110000110001000110100011101101100111111100000100001000001011110010101011100110011011100100001101110101010101011100000010010101011110100001000110101111000001101100100100100010100000000011101110110111010010100001100101101101111001000110110100010000100100001110101110000111100001010100111101000010010100001111101011000010001110010000101110001011111101000001100110011000101010101101001000101100110111000110000110000000011101001110100000100000110001101101100010100010101001110100001001100110010101000000001110111000001111101111001000101101001010111010111000100001111110110111110101111010100011111011100010000000100100101010000000000110010101100001000000001010111100100010111010001010011100100100001100101101111110101101001110100111010110000001000100110101000000101000110100001011000011101000110100110110010000101000010100100011110111100111011111000011111001000010101010101011000000011011110111110010110"



from functools import reduce

def hamming_74_feilkorrigerende(bits):
    bits = list(map(int, bits))
    for i in range(0, len(bits), 7):
        # finner bitpos (+1 fra python array index) som har bit satt (1)
        bitpos = [(1+i) for i,bit in enumerate(bits[i:i+7]) if bit == 1]                
        if bitpos:
            # xor'er bitposisjonene sammen, og hvis svaret blir >0 så har vi en feil,
            # og vi vet hvilket bit som er feil. vi flipper bittet for å feilkorrigere.            
            biterror = reduce(lambda a,b: a^b, bitpos)
            if biterror > 0:
                bits[i+(biterror-1)] ^= 1 # xor med 1 flipper et bit        
    return "".join(list(map(str, bits)))

print(print_bits(bits))
bits = hamming_74_feilkorrigerende(bits)
print(print_bits(bits))
# heksectf{EeikloRrgGmRmLDe_KoBfa@rU}EtaI_KCC_myNfegAg_lQL`wpodAgecoicRaTpc_sLjasfUi\gFOR_JaoSNmii?lCpbKOaAvxoJO~}
# helsectf{FeilkoRriGereNDe_KoDe_BrUkEs_I_ECC_miNne_Og_kAN_oPpdAge_oG_RetTe_eNbitfEiL_fOR_Aa_UNnga_dAtaKOrRupsJOn}