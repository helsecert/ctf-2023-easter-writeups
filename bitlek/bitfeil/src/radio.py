import random
def introduser_bitfeil(bits,bitfeilrate): 
    # denne funksjonen er egentlig ikke en del av oppgaven, og ble utelatt for å unngå at
    # man bruker tid her.
    #
    # den simulerer en-bitsfeil (bittet blir flippet) 75% av gangene for hver 7bits del av meldingen.
    assert len(bits)%7 == 0
    bits = list(map(int,bits))
    for i in range(0, len(bits), 7):
        if random.randint(0,100) > bitfeilrate:
            continue
        err_bit = random.choice([0,1,2,3,4,5,6]) # velger hvilket bit som skal få en feil.
        bits[i+err_bit] ^= 1 # xor med 1 flipper et bit (0 -> 1, 1 -> 0)
    return "".join(list(map(str, bits)))

def overf0r_bits(bits, bitfeilrate):
    return introduser_bitfeil(bits, bitfeilrate)