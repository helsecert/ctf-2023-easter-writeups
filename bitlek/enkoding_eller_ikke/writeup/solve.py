"""
For vanlig 7bits ascii er det bare å shifte 7 og 7 bit for å hente ut plaintext. Et ASCII tegn har fra før MSB lik 0 og derfor kan vi si at en 8bite byte er 7bits ASCII siden MSB alltid er null.

I denne oppgaven XORer vi med 128 (= 0b1000000). Vi får derfor XOR 0^1=1 i MSB, dvs vi setter MSB til 1. Øvrig 7bit i byten er uforandret. Når vi da shifter 7bit med 8bit data får vi overlapp mellom MSB i en byte og LSB i neste byte. Overlappen blir OR'et sammen.
  <8bitxx>
         <8bitxx>
         |      <8bitxx>
         |      |
         ^      ^
      Her får vi LSB og MSB OR'et sammen

Bitoperasjonene AND og OR er destruktive, og når vi OR'er med 1 kan vi ikke finne tilbake LSB sin opprinnelige verdi: `1 | LSB = 1`. Mens XOR er ikke-destruktiv og er sin egen invers (A^A=0).

== Oppsummert; det som skjer i oppgaven er at vi ødelegger LSB bittene til alle ASCII tegnene.

Det er derfor umulig å direkte dekryptere plaintexten (kanskje hvis vi hadde ei ordbok?) og vi må heller prøve oss frem. Oppgaven er sånn sett litt slem, men veldig løsbar :)

== Løsning

Siden vi har XORet med 128 så kunne man tenke seg at man kan XORe med 128 på nytt for å dekryptere hver byte. Men det går ikke her, siden vi knuser LSB bittet. Vi kan forsøke  å XOR'e med 128, og se hvordan det ser ut:
  èåíóåãõçûÏÓßåóßåïßååóõóõëõé÷ßïñåóáóëïïý\x81

Vi kan maske 0x7f for å kun få printable ascii:
  hemsecug{OS_es_eo_eesusukuiw_oqesaskoo}\x01

Vi får nå ut en delvis klartekst som ligner på et flagg, men hvor LSB bittet på noen av tegnene er feil.

Hvis vi ser på den delvise klarteksten ser vi at vi har fått en ekstra byte på slutten som kun har LSB
satt (\x01 = 0b00000001 = LSB satt). Dette er et hint om hva som skjer i oppgaven siden MSB fra siste
klartekst byte blir OR'et med LSB i neste byte som er null/tom, og derfor får vi bare LSB bit satt.
Vi kan derfor se bort fra denne \x01.

I den delvise klarteksten over kan vi forvente at ca 50% av tegnene er ødelagt og 50% er beholdt, hvis vi har en god fordeling av ascii tegn og ikke veldig mye gjenbruk.

Vi kan sjekke ASCII tabellen for å finne hvilke tegn som ikke endres når LSB blir satt til 1:
>>> import string
>>> "".join([x for x in string.printable if ord(x)&1 == 1])
"13579acegikmoqsuwyACEGIKMOQSUWY!#%')+-/;=?[]_{}\t\r\x0b"

Mens disse tegnene får byttet om LSB:
>>> "".join([x for x in string.printable if ord(x)&1 == 0])
'02468bdfhjlnprtvxzBDFHJLNPRTVXZ"$&(*,.:<>@\\^`|~ \n\x0c'

Vi ser at _{} ikke blir endret (de har bit1 satt) og disse er i vår delvise klartekst, mens bokstaver som
`ltf` vil begge bli gjort om til `mug`. Dette stemmer også bra sier vi ser at "helsectf" blir til "hemsecug{". 

Det som kan hjelpe oss er at når LSB flippes så vil et tegn kun endres til nabo-tegnet, som når "l" blir "m":
```
>>> bin(ord("l"))
'0b1101100'         <- lsb = 0
>>> bin(ord("m"))
'0b1101101'         <- lsb = 1
```
Dette reduserer antall kombinasjoner per tegn som vi trenger å sjekke. En `m` er enten en `m` eller en `l`.

Vi ser at `_{}` allerede har LSB satt, og gitt den delvise klarteksten `hemsecug{OS_es_eo_eesusukuiw_oqesaskoo}\x01` så gjenkjenner vi flagformatet `helsectf{..}`.

Vi kan nå permutere ut kombinasjoner av "ordet" splittet med underscore for å se om vi kan bygge opp en gjenkjennelig setning.

O  S
N  R

e  s
d  r

e  o
d  n

e  e  s  u  s  u  k  u  i  w
d  d  r  t  r  t  j  t  h  v

o  q  e  s  a  s  k  o  o
n  p  d  r  `  r  j  n  n

Nå er det tipping og gjenkjenning av ord som er viktig. Riktig svar var:

    OR
    er
    en
    destruktiv
    operasjon
    
Riktig flagg ble:

    helsectf{OR_er_en_destruktiv_operasjon}
"""
exec(open("../src/output.txt").read())
ct3 = ct

pt3=""
c3=ct3
while c3 > 0:
    pt3 += chr((c3&127))
    c3 >>= 7
print([pt3]) # hemsecug{OS_es_eo_eesusukuiw_oqesaskoo}\x01
pt3=pt3[:-1]

from itertools import permutations

# splitter ordene med _ og printer ut to og to mulige tegn per posisjon 
# enten med LSB satt til 1 eller ikke (^1 = XOR 1 = inverterer bittet)
plaintext = list(b"helsectf{" + b"?"*(len(pt3)-5) + b"}")
for word in pt3[9:-1].split("_"):
    print("  ".join(word))
    print("  ".join([chr(ord(w)^1) for i,w in enumerate(word)]))        
    print()
