# Oppgaven ble laget slik:

from Crypto.Util.number import bytes_to_long,long_to_bytes
tekst = """In computing, bit numbering is the convention used to identify the bit positions in a binary number. 

In computingthe most significant bit (MSB) represents the highest-order place of the binary integer. The LSB is sometimes referred to as the low-order bit or right-most bit, due to the convention in positional notation of writing less significant digits further to the right. The MSB is similarly referred to as the high-order bit or left-most bit. 

Source: https://en.wikipedia.org/wiki/Bit_numbering
""".encode()
flag = b"helsectf{ubrukt_MSB_er_bortkastet_bit}"
flag = bytes_to_long(flag)
ciphertext = list(tekst)
for i in range(flag.bit_length()):
    ciphertext[i] |= 128 * ((flag>>i)&1)
ciphertext = bytes(ciphertext).hex()
print(ciphertext)

