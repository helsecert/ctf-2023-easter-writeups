"""
Tegnene i ASCII er alltid 7 bit, og det 8'ende bittet (som er "lengst til venstre") er alltid satt til 0.

Bitrens funksjonen i oppgaven masker hvert tegn med en AND 0x7f (0b01111111) slik at vi kun får ut gyldig 7bits ASCII. MSB blir da satt til 0.

Men hva ligger i MSB? Ofte kan man bruke det 8'ende bittet for å lagre paritet på de øvrige 7 bittene for å detektere feil.

I oppgaven ligger flagget lagret som en bitstrøm i MSB til hvert tegn. Vi henter ut MSB fra hver byte for å få en lang bitstrøm. Dette er flagget.

Hvis man er usikker på hvilken "vei" bittene er enkodet, så vi kan jo prøve begge veier. I python snus/reverseres en streng/array med [::-1]
"""
from Crypto.Util.number import bytes_to_long,long_to_bytes
melding = "c96ea0e3efedf0757469ee67aca0e269f4206ef56de2e57269ee672069f3a074e8e5a0e3ef6ef6656e74e96feea0f573e564a0746fa0e964656ef469e6f9a074e8e52062e9f4a070ef73697469efee73a0e96ea061a0e2696e61f279a0eef56d62e5722ea08a8a49eea0e3ef6df0f57469ee677468e5a06deff3f4a0f369e76e69e66963e1eef420e269f42028cdd342a9a0f2e5f072e57365ee74732074e865a0e86967e865f374ad6ff2e46572a070ece1e3e5a06fe6207468e520e2e9ee61f2f920e96ef4e567e572ae20d4e8e5204cd34220e9f3a0736fed657469ede573a072e566e5f2f265e4a074efa0e1f32074e8e5206ceff72d6f72e465f2a0e269f4a06f7220f2e967e874ad6d6ff3f420e2e9742ca0e4f5652074efa074e8e520e36fee7665eef4696f6e20e96ea0f06f736974696f6e616c206e6f746174696f6e206f662077726974696e67206c657373207369676e69666963616e7420646967697473206675727468657220746f207468652072696768742e20546865204d53422069732073696d696c61726c7920726566657272656420746f2061732074686520686967682d6f7264657220626974206f72206c6566742d6d6f7374206269742e200a0a536f757263653a2068747470733a2f2f656e2e77696b6970656469612e6f72672f77696b692f4269745f6e756d626572696e670a"
bits = "".join(map(lambda bit: str(bit>>7),  bytes.fromhex(melding))) # 10111110001011101001...
print(long_to_bytes(int(bits,2)))
print(long_to_bytes(int(bits[::-1],2)))
